---
title: OpenFlexure Forum
hero: '/assets/icons/discourse.svg'
link: 'https://openflexure.discourse.group/'
margin: margin-remove
---

**New to our group? Check out our forum to see what the community is talking about!**

Join the discussion and see our announcements! Get help, request features, and share knowhow on the OpenFlexure Forum.
