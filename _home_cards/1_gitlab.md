---
title: Join us on GitLab
hero: '/assets/icons/gitlab-icon-rgb.svg'
link: 'https://gitlab.com/openflexure'
margin: margin-left-very-small
---

<br>
For developers, all of our designs, source-code, and support can be found on our GitLab group.
<br>
<br>
