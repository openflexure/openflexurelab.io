---
layout: page
title: Interactive view
permalink: /projects/deltastage/interactive-view
sitemap: false
---

<p align="center">
<gitbuilding-viewer src="{{ '/assets/renders/deltastage.glb' | absolute_url }}" class="gbview"></gitbuilding-viewer>
</p>
