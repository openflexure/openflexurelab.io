---
layout: page
title: Build a Delta Stage
permalink: /projects/deltastage/build
sitemap: false
---

<script>
// script to redirect the 'Install the OS' anchor link to the install page.
(function () {
    var anchorMap = {
        "install-the-os": "/projects/microscope/install#raspbian-openflexure",
    }
    var hash = window.location.hash.substring(1);
    if (hash) {
        window.location.replace(anchorMap[hash]);
    }
})();
</script>

To download, print and build a Delta Stage, follow our assembly instructions below.

Before you start building, you may find it useful to look at the [interactive view of the delta stage](./interactive-view.md).

<div class="flex-container-tight">

  <div>
    <div>
      <img src="../../assets/ds-photos/ds_v1.2.0_close.jpg" width = "400">
    </div>
  </div>

  <div>
    <h3>OpenFlexure Delta Stage v1</h3>
    <p><a target="_blank" class="download-btn" href="https://build.openflexure.org/openflexure-delta-stage/latest/">Assembly instructions</a></p>
  </div>

</div>

### Source

You can view the source files on [Gitlab](https://gitlab.com/openflexure/openflexure-delta-stage).

### Legacy versions

You can view legacy versions of the Delta Stage on the [build server](https://build.openflexure.org/openflexure-delta-stage/).


## Next steps

### [Install the Software](./install)
