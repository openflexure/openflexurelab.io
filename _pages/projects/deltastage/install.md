---
layout: page
title: Install the Software
permalink: /projects/deltastage/install
sitemap: false
---

## Raspbian-OpenFlexure

{% include software/install-raspbian.md %}

## OpenFlexure Connect

{% include software/install-openflexure-connect.md %}

## Next steps

### [Use your Delta Stage](./control)
