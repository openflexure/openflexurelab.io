---
layout: page-centered
title: OpenFlexure Delta Stage
permalink: /projects/deltastage/
sitemap: true
---

![Delta Stage Microscope](/assets/ds-photos/ds_v1.2.0_wide.jpg "Delta Stage Microscope"){: .image-xx-large .no-shadow .float-center}
<div class="flex-container">

  <div class="padding-small text-center">
    <a href="./build">
        <div>
            <i class="material-symbols-outlined big-icon">build</i>
            <h4>Build a Delta Stage</h4>
        </div>
    </a>
    <div class="hint-text">
        A guide to printing and assembling an OpenFlexure Delta Stage.
    </div>
  </div>

  <div class="padding-small text-center">
    <a href="./install">
        <div>
            <i class="material-symbols-outlined big-icon">developer_board</i>
            <h4>Install the Software</h4>
        </div>
    </a>
    <div class="hint-text">
        Download and install software to control your Delta Stage.
    </div>
  </div>

  <div class="padding-small text-center">
    <a href="./control">
        <div>
            <i class="material-symbols-outlined big-icon">computer</i>
            <h4>Use your Delta Stage</h4>
        </div>
    </a>
    <div class="hint-text">
        Get started using your Delta Stage.
    </div>
  </div>

</div>


## OpenFlexure Delta Stage
{: .text-center}

The [microscope] works nicely for simple microscopy, with 12mm lateral travel and 4mm travel in the focus direction.  However, the optics module moves - that's less than ideal if you want to use more complicated optics than just bright field.  The [block stage] has a single stage that moves in X, Y, and Z, but it's really just designed for fine motion - its 2mm range is less than we'd like for microscopy.  The delta stage aims to solve this, by moving the sample in 3D with a mechanism very similar to the microscope's.  That should allow it to get similar range of travel, but with totally static optics.  We strongly suspect there will also be other fun things to do with it that don't involve microscopes at all.

## Where can I find it?
{: .text-center}

The current version of the delta stage lives on our [Gitlab repository]. You can download the STL files from our [build server] but first take a look at the [assembly instructions] as you do not need to print every part.

## Publications

Our paper describing all of our design decisions and the current functionality of the OpenFlexure Delta Stage Microscope can be found in [Optics Express]. 

It has been presented at [RMS Virtual Frontiers in BioImaging 2020], with a [pre-recorded talk]. The stage has also been used up in some very nice work by the Cambridge Sensors CDT students on an Optical Projection Tomography system which is described in [Scientific Reports] and [Lab on the Cheap]. More projects which have used the Delta Stage can be seen in the [publications page].  

If you use the delta stage, please let us know in the [forum]! 


[microscope]: /projects/microscope/
[block stage]: /projects/blockstage/
[build server]: https://build.openflexure.org/openflexure-delta-stage/
[assembly instructions]: https://build.openflexure.org/openflexure-delta-stage/latest/
[Gitlab repository]: https://gitlab.com/openflexure/openflexure-delta-stage/
[RMS Virtual Frontiers in BioImaging 2020]: https://www.rms.org.uk/frontiers-in-bioimaging-2020-home.html
[pre-recorded talk]: https://youtu.be/RMnBPtE2gu0
[Lab on the Cheap]: https://www.labonthecheap.com/optij-open-source-optical-projection-tomography-for-3000/
[Scientific Reports]: https://doi.org/10.1038/s41598-019-52065-0
[Optics Express]: https://doi.org/10.1364/OE.450211
[forum]: https://openflexure.discourse.group/
[publications page]: /about/media-publications