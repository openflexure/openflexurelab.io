---
layout: page
title: Build a Microscope
permalink: /projects/microscope/build
sitemap: false
---
<script>
// script to redirect the 'Install the OS' anchor link to the install page.
(function () {
    var anchorMap = {
        "install-the-os": "/projects/microscope/install#raspbian-openflexure",
    }
    var hash = window.location.hash.substring(1);
    if (hash) {
        window.location.replace(anchorMap[hash]);
    }
})();
</script>

To build a standard microscope configuration, follow the assembly instructions below, which includes the download for the STL files.

<img src="{{ '/assets/ofm-photos/v7_illumination_screw.jpg'| absolute_url }}" class="float-right small" width = "200">

### OpenFlexure Microscope v7

v7 of the microscope is now in Beta. This means it is not a fully polished release, but we already believe it's much, much better than v6. We recommend you use v7 unless you have a particular reason to prefer v6.

<a target="_blank" class="download-btn" href="https://build.openflexure.org/openflexure-microscope/v7.0.0-beta3/">Instructions and STL files for v7</a>

Before you start building, you may find it useful to look at the [interactive view of the microscope](./interactive-view-v7.md).

Full release notes are available on [GitLab](https://gitlab.com/openflexure/openflexure-microscope/-/releases), but the headline features are:

* Rewritten instructions, with accurate bill of materials and rendered diagrams throughout.
* Fully enclosed wiring.
* No M3 screws threading into plastic, to eliminate stripped threads and damaged parts.
* Improved illumination mounting, and PCB option to eliminate wire-to-wire soldering.
* Standardised hardware: fewer types of screw required.
* Better optics module mounting to get the objective at the right height.

Under the hood, there has also been an extensive rewrite of the OpenSCAD code to make it more reliable and maintainable for the future. It's the best OpenFlexure Microscope yet!

### OpenFlexure Microscope v6

The previous version of the microscope is still accessible on the build server.  
First, <a target="_blank" href="https://microscope-stls.openflexure.org">configure and download STLs</a>, 
then follow the <a target="_blank" href="https://build.openflexure.org/openflexure-microscope/v6.1.5/docs">assembly instructions</a>. There is also an [interactive view of v6.1.5](./interactive-view-v6.md).

### Source

You can view the source files on [Gitlab](https://gitlab.com/openflexure/openflexure-microscope).

### Legacy versions

You can view legacy versions of the microscope on the [build server](https://build.openflexure.org/openflexure-microscope/).

## Next steps

### [Install the Software](./install)
