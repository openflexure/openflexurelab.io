---
layout: page
title: Install the Software
permalink: /projects/microscope/install
sitemap: false
redirect_from: /projects/microscope/build#install-the-os
---

## Raspbian-OpenFlexure

{% include software/install-raspbian.md %}

## OpenFlexure Connect

{% include software/install-openflexure-connect.md %}

## Next steps

### [Use your Microscope](./control)
