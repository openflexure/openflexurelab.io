---
layout: page-centered
title: OpenFlexure Microscope
permalink: /projects/microscope/
sitemap: true
---

<div class="row" style="align-items:center;">
  <div class="flex-container column" style="flex-direction:column" >
    <div class="padding-v-small text-center">
        <a href="./build">
            <div>
                <i class="material-symbols-outlined fairly-big-icon">build</i>
                <h4>Build a Microscope</h4>
            </div>
        </a>
        <div class="hint-text">
            A guide to printing and assembling an OpenFlexure Microscope.
        </div>
    </div>

    <div class="padding-v-small text-center">
        <a href="{{ '/about/vendors' | absolute_url }}">
            <div>
                <i class="material-symbols-outlined fairly-big-icon">shopping_cart</i>
                <h4>Buy a Microscope</h4>
            </div>
        </a>
        <div class="hint-text">
            Buy a microscope or kit from a vendor near you.
        </div>
    </div>

    <div class="padding-v-small text-center">
        <a href="./install">
            <div>
                <i class="material-symbols-outlined fairly-big-icon">developer_board</i>
                <h4>Install the Software</h4>
            </div>
        </a>
        <div class="hint-text">
            Download and install software to control your microscope.
        </div>
    </div>

    <div class="padding-v-small text-center">
        <a href="./control">
            <div>
                <i class="material-symbols-outlined fairly-big-icon">computer</i>
                <h4>Use your Microscope</h4>
            </div>
        </a>
        <div class="hint-text">
            Get started using your microscope.
        </div>
    </div>
  </div>

  <div class="padding-v-small text-center column">
    <img src="{{ '/assets/ofm-photos/v7_complete.jpg' | absolute_url }}" alt="The OpenFlexure Microscope v7.0.0-alpha1" class=".image-fairly-large no-shadow" />
  </div>

</div>

## Microscopy for everyone
{: .text-center}

The OpenFlexure Microscope is a customisable, open-source optical microscope, using either very cheap webcam optics or lab quality, RMS threaded microscope objectives.  It uses an inverted geometry, and has a high quality mechanical stage which can be motorised using low cost geared stepper motors.


An overview paper of the OpenFlexure Microscope is [available freely from bioRxiv](https://www.biorxiv.org/content/10.1101/861856v1){:target="_blank"}.

The original paper describing the design is available open-access from [Review of Scientific Instruments](http://dx.doi.org/10.1063/1.4941068){:target="_blank"}. You can read various [media articles](/about/media-publications) about it for a more user-friendly introduction.


## High-performance, low-cost
{: .text-center}

Optomechanics is a crucial part of any microscope; when working at high magnification, it is absolutely crucial to keep the sample steady and to be able to bring it into focus precisely.  Accurate motion control is extremely difficult using printed mechanical parts, as good linear motion typically requires tight tolerances and a smooth surface finish.  

This design for a 3D printed microscope stage uses plastic flexures, meaning its motion is free from friction and vibration.  It achieves steps well below 100 nanometers when driven with miniature stepper motors, and is stable to within a few microns over several days.

## Easy to source, quick to assemble
{: .text-center}

This design aims to minimise both the amount of post-print assembly required, and the number of non-printed parts required - partly to make it as easy as possible to print, and partly to maximise stability; most of the microscope (including all the parts with flexures) prints as a single piece.  

The majority of the expense is in the Raspberry Pi and its camera module; the design requires only around 200g of plastic and a few nuts, bolts and other parts.  

## Lab-ready customisation
{: .text-center}

The optics module (containing the camera and lens) can be easily swapped out or modified, for example to change the magnification/resolution by using a microscope objective.

Optional filter cubes can be printed, allowing for reflection (epi-) illumination, polarisation-contrast microscopy, and even fluorescence imaging.
