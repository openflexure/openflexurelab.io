---
layout: page
title: Interactive exploded view of v6.1.5
permalink: /projects/microscope/interactive-view-v6
sitemap: false
---
<p align="center">
<gitbuilding-viewer src="{{ '/assets/renders/microscope.glb' | absolute_url }}" class="gbview"></gitbuilding-viewer>
</p>
