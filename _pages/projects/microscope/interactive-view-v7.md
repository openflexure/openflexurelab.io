---
layout: page
title: Interactive 3D view
permalink: /projects/microscope/interactive-view-v7
redirect_from: /projects/microscope/interactive-view
sitemap: false
---
<p align="center">
<gitbuilding-viewer src="{{ '/assets/renders/v7.0.0-beta1-highres.glb' | absolute_url }}" class="gbview"></gitbuilding-viewer>
</p>
