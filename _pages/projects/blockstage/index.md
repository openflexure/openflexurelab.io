---
layout: page-centered
title: OpenFlexure Block Stage
permalink: /projects/blockstage/
sitemap: true
---

![OpenFlexure Block Stage](/assets/bs-photos/bs_wide.jpg){: .image-xx-large .no-shadow .float-center}


## High-precision, 3-axis translation stage

The OpenFlexure Block Stage is a 3D printable design that enables very fine (sub-micron) mechanical positioning of a small moving stage, with surprisingly good mechanical stability. You can read about it in the [Optics Express article](https://doi.org/10.1364/OE.384207) (open access).  It follows on from the [OpenFlexure Microscope](/projects/microscope/) which is discussed in a [paper in Review of Scientific Instruments](http://dx.doi.org/10.1063/1.4941068) (open access). Currently, it's designed to function as a more-or-less drop in replacement for fibre alignment stages available from various scientific suppliers, but the project aims to be useful to electrophysiologists, nanotechnology folk, and many more.

You can see all the development on the [gitlab repository](https://gitlab.com/openflexure/openflexure-block-stage/) and download the STL files from  inside the assembly instructions.

<p><a target="_blank" class="download-btn" href="https://build.openflexure.org/openflexure-block-stage/v1.1.0/">Assembly instructions</a></p>
