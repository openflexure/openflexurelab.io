---
layout: page
title: OpenFlexure projects
permalink: /projects/
sitemap: false
---

<div class= "flex-container">
    <div class="flex-grow flex-container flex-container-vertical">
        {% for home_card in site.home_cards_devices %}
            {% include homecard.html link=home_card.link title=home_card.title content=home_card.content hero=home_card.hero%}
        {% endfor %}
    </div>
</div>