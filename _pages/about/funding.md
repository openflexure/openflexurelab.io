---
layout: page
title: Funding
permalink: /about/funding
sitemap: true
---

 project has been made possible by the generous support of a number of funders:

<div markdown="1" class="funders-container">
<div markdown="1" class="funder-container">
![Royal Commission for the Exhibition 1851](/assets/about/funders/RoyalCommission.png){: .image-funders}
</div>
<div markdown="1" class="funder-container">
![EPSRC](/assets/about/funders/EPSRC.svg){: .image-funders}
</div>
<div markdown="1" class="funder-container">
![GCRF](/assets/about/funders/GCRF.png){: .image-funders}
</div>
<div markdown="1" class="funder-container">
![UKRI](/assets/about/funders/UKRI.png){: .image-funders}
</div>
<div markdown="1" class="funder-container">
![The Royal Society](/assets/about/funders/RoyalSociety.svg){: .image-funders}
</div>
<div markdown="1" class="funder-container">
![NIHR](/assets/about/funders/NIHR.png){: .image-funders}
</div>
<div markdown="1" class="funder-container">
![MRC](/assets/about/funders/MRC.png){: .image-funders}
</div>
<div markdown="1" class="funder-container">
![ProSquared Network](/assets/about/funders/ProSquared.png){: .image-funders}
</div>
<div markdown="1" class="funder-container">
![University of Cambridge](/assets/about/funders/Cambridge.svg){: .image-funders}
</div>
<div markdown="1" class="funder-container">
![University of Bath](/assets/about/funders/Bath.svg){: .image-funders}
</div>
<div markdown="1" class="funder-container">
![University of Glasgow](/assets/about/funders/UoG.png){: .image-funders}
</div>
<div markdown="1" class="funder-container">
![MIT Solve](/assets/about/funders/MIT_Solve.png){: .image-funders}
</div>
<div markdown="1" class="funder-container">
![Johnson & Johnson Foundation](/assets/about/funders/J&J.png){: .image-funders}
</div>
<div markdown="1" class="funder-container">
![CAP Foundation](/assets/about/funders/capf_logo.svg){: .image-funders}
</div>
</div>


 <details markdown="1">
* UKRI:
    * [EP/T029064/1](https://gtr.ukri.org/projects?ref=EP%2FT029064%2F1) "Digital diagnostics for smarter healthcare in Africa"
* EPSRC:
    * [EP/P029426/1](https://gtr.ukri.org/projects?ref=EP%2FP029426%2F1) "Open Lab Instrumentation"
    * [EP/R013969/1](https://gtr.ukri.org/projects?ref=EP%2FR013969%2F1) "Detailed malaria diagnostics with intelligent microscopy" (Co-funded by NIHR)
    * [EP/R011443/1](https://gtr.ukri.org/projects?ref=EP%2FR011443%2F1) "Parallel live microscopy for high-throughput malaria research"
* The Royal Society:
    * URF\R1\180153
    * RGF\EA\181034
    * PEF\R3\3026
* [ProSquared Network](https://prosquared.org/2024/09/09/new-round-of-funded-projects/)
* MRC Confidence in Concept award
* The Royal Commission for the Exhibition of 1851
* The University of Cambridge
* The University of Bath
* The University of Glasgow
* College of American Pathologists (CAP) Foundation - 2023 Global Pathology Development Award
* [MIT Solve - Global Health Equity Challenge Solvers 2024](https://solve.mit.edu/solutions/87685)
* Johnson & Johnson Foundation - Health Equity Innovation Award 2024
</details> 