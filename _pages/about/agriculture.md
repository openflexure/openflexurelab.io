---
layout: page
title: Agriculture
permalink: /about/agriculture
sitemap: true
---

![Soil health microscopy with OpenFlexure](/assets/about/soil_health.jpg "Soil health microscopy with OpenFlexure"){: .image-medium .float-right}

<b> Responsible agricultural practices are vital for the future of farming, not just for the coming years, but coming generations. A key component of responsible farming is ensuring soil health is maintained. Microscopy allows farmers to see the organisms that indicate that well-maintained, properly-used soil. </b>

Fernando "Nano" Castro and others at the Open Agroecology Lab have been using OpenFlexure Microscopes to study the soil ecosystem in Mendoza, Argentina. Digital microscopy opens up the microscopic world of organisms including mites, nematodes and tardigrades. Introducing students and farmers to this microscopic world can change how they view soil, and can help promote sustainable farming practices. Using a portable OpenFlexure Microscope within the community helps engagement with the results and messages, rather than feeling removed from the testing.

A growing community is using OpenFlexure to monitor their soil health and promote responsible agriculture. Guides on how to get started with soil microscopy are now available in both [English](/assets/about/Soil_Microscopy_Guide_EN.pdf) and [Spanish](/assets/about/Soil_Microscopy_Guide_ES.pdf).

<b> Want to see more? Check out the talk from Fernando Castro at [OpenFlexureCon2022](/openflexurecon2022/community_stories/2022/10/20/openflexurecon-castro) </b>

![OpenFlexure workshop for science teachers in Sweden](/assets/about/soil-workshop.jpg){: .image-100}
<i>An OpenFlexure Micoroscope and an OpenFlexure block stage being used to view soil samples. Source: Open Agroecology Lab<i>


[Return to index](/about/index)
