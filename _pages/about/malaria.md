---
layout: page
title: Malaria
permalink: /about/malaria
sitemap: true
---

![Joram at IHI using the OpenFlexure Microscope to image blood smears.](/assets/about/joram.png "Joram at IHI using the OpenFlexure Microscope to image blood smears."){: .image-medium .float-right}

<b> Malaria is a parasitic infection which kills over 500,000 people per year. The World Health Organization identifies light microscopy as the "gold standard" of malaria diagnosis, but this requires high-quality microscopes and trained technicians. We've worked for years to see exactly where the OpenFlexure Microscope can support this work. </b>

In collaboration with the Ifakara Health Institute (Bagamoyo, Tanzania) and Bongo Tech & Research Labs (Dar Es Salaam, Tanzania), we've been investigating the screening of blood samples for malaria parasites. As an automated digital microscope, the OpenFlexure Microscope enables better record as a digital representation of each slide can be captured. These digital records enable improved quality assurance and training. This allows medical students to be trained on digital images of representative samples from their own community.

As the OpenFlexure Microscope can be built and maintained locally, this can transform the procurement of healthcare equipment in low-resource regions. Our engineering partners at Bongo Tech & Research Labs (BTech) have produced the microscopes used in this study, which were then used by collaborators at the Ifakara Health Institute (IHI) to image stained blood smears. We have amassed a large dataset of blood smear images, and demonstrated the potential for machine learning to screen such samples for normal and abnormal cells.

We are working with BTech towards the eventual goal of certification, for manufacturing microscopes for medical use in Tanzania. Other partners, including Mboalab in Cameroon, are investigating similar projects in their regions.

<b> Want to read more? Check out talks from Joram Mduda (IHI) and Paul Nyakyi (BTech) at [OpenFlexureCon2022](/openflexurecon2022/community_stories/2022/10/20/openflexurecon-tanzania) </b>

<b> Or read the WHO's recommendation in their [2024 compendium of innovative health technologies for low-resource settings](https://www.who.int/publications/i/item/9789240095212) (page 69). </b>

[![WHO compendium of innovative health technologies for low-resource settings 2024. With an OpenFlexure Microscope on front cover.](/assets/about/WHO_Compendium_2024.png){: .image-large  .float-center}](https://www.who.int/publications/i/item/9789240095212)

[Return to index](/about/index)
