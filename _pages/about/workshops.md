---
layout: page
title: Workshops
permalink: /about/workshops
sitemap: true
---

![An AfricaOSH workshop building OFMs](/assets/about/workshop.jpg "An AfricaOSH workshop building OFMs"){: .image-medium .float-right}

<b> An open project is only as strong as its community, and the OpenFlexure community is being constantly expanded by some incredible workshops. </b>

Core to the OpenFlexure ethos is being able to build, use and maintain your own microscope. Although we put a lot of work into our documentation, there's no replacement for building your first OpenFlexure Microscope with an expert. 

Our partners at [AfricaOSH](https://africaosh.com/) and the [West African Microscopy and Bio-Imaging Analysis Network (WAMBIAN)] had run OpenFlexure workshops universities in Ghana and Sengal. These workshops supply the parts and know-how to allow microscopists to build their own OpenFlexure Microscopes. In addition to increasing the accessibility of microscopes in the area, these courses also teach key skills including image analysis, and collaboration.

In Sothern Ukraine [Tolocar](https://tolocar.org/) has run workshops in collaboration with [Mykolaiv Water Hub](https://mykolaivwaterhub.com/). These workshops were run for water testing professionals and local college students -- they focused on public health applications of locally produced OpenFlexure microscopes.

OpenFlexure community member [Per Wilhelmsson](https://pkiw.github.io/) has also run workshops for [high school teachers in Sweden](https://www.openscienceshop.org/building-open-science-hardware-with-secondary-school-teachers/). Each teacher left with a calibrated OpenFlexure Microscope, and a new knowledge and enthusiasm for building their own science hardware.

<b> Inspired to run your own workshop? Hoping to attend one in the future? Have one to tell us about? Get in touch with us on our [forum](https://openflexure.discourse.group/) </b>

<b> Or to read about the workshops run so far, check out [Pez and Elena's discussion](https://www.mdc-berlin.de/news/news/microscopy-workshops-ghana) of their experiences, lessons learned, and future goals.

![OpenFlexure workshop for science teachers in Sweden](/assets/about/workshop-sweden.jpg){: .image-100}
<i>An OpenFlexure workshop for science teachers in Sweden. Source: Per Wilhelmsson<i>

[Return to index](/about/index)
