---
layout: page
title: Need help?
permalink: /about/need-help
sitemap: true
accordion: 
  - title: Can I have the CAD files in another format?
    content: The OpenFlexure Microscope is developed in OpenSCAD, an open source script-based CAD package.  We automatically compile the project into print-ready `STL` files, but the original editable `scad` files are available in our [repository](https://gitlab.com/openflexure/openflexure-microscope/).  OpenSCAD renders the designs directly from code into meshes, which means we never have a BREP version of the models.  Unfortunately, this means that CAD packages that won't import STL files are unlikely to be able to import the design.
  - title: Why did you choose OpenSCAD?
    content: There are several reasons we use OpenSCAD.  OpenSCAD means we have a simple, text-based file format that's easy to inspect and version-manage, using standard tools developed for software code.  This is incredibly useful when trying to manage the project as it continues to evolve.  It also enables us to re-use designs easily, for example in the delta stage and block stage, both of which use parts of the original microscope design.  OpenSCAD fits neatly into our fully open source toolchain, allowing us to automatically rebuild the project every time it is updated, and making sure nobody is excluded because they can't afford an expensive CAD license.  It is based on a different CAD principle to most commercial packages - constructive solid geometry rather than boundary representation.  This works very nicely for the complicated 3D shapes required by the OpenFlexure mechanisms, and allows us to preserve the design intent in the source code as well as the final shape.  This would be very hard to do in most commercial CAD packages.  Finally, Richard started the project in 2015 as a programmer rather than a CAD modeller, meaning he was able to work much more effectively in OpenSCAD than any of the more conventional CAD packages.
  - title: Can I buy a kit or an assembled microscope?
    content:  Yes! A number of manufacturers produce the microscope as part of the Open Science Shop inititive. Please see our [vendors page](/about/vendors).
  - title: Can you help me with a technical question?
    content: The best place to find support is on the [forum](https://openflexure.discourse.group/).  If you ask for support by email, please don't be offended if we suggest you use the forum instead.  This enables more people to see your question, which gets you a faster response and most likely a better one.  It also means that the questions and answers build up into a useful, searchable resource for others in the future.
  - title: I want to contribute, but I can't code or work on the design - what can I do?
    content: A great place to start is just to post about what you're doing.  Having an active community in the forum is both really fun and very useful - it allows the core team to show our funders that the project is having an impact.  Build reports are amazingly helpful to us as we refine the instructions, and also to others as they try to figure out how to do particular steps. Posting images and suggestions of things to look at is really helpful for people using microscopes in educational settings.
  - title: Can I sell it or use it in my commercial product?
    content: Yes! The CERN open hardware license permits you to make, sell, and modify the design provided you share your modified version under the same license.  If you're using any hardware or software from the project commercially, it's enormously helpful if you can let us know either through the forum or by email.  We are happy to treat any information you give us as confidential, but it's really useful to help our funders evidence the "impact" of our research.
  - title: Is the OpenFlexure Microscope a medical device?
    content: See our page on [medical devices](/about/medical-devices).
  - title: Can I pay you to add a feature?
    content: Yes! We are committed to keeping the project open, but some members of the team have undertaken consultancy to meet particular needs.  This is usually easier to do if you're happy for anything that's developed to be put back into the open project. Please get in touch by email if you'd like to discuss this.

---

<div class="flex-container-tight">
    {% for home_card in site.home_cards %}
        {% if home_card.title == "OpenFlexure Forum" %}
            {% include homecard.html link=home_card.link title=home_card.title content=home_card.content hero=home_card.hero%}
        {% endif %}
    {% endfor %}
</div>

## Frequently Asked Questions

{% include accordion.html %}

## Raise an issue

If you find something is broken, there's a mistake in the instructions, or you want to suggest a feature, please let us know by [raising an issue on Gitlab](https://gitlab.com/openflexure/openflexure-helpdesk/-/issues/new) or posting in the [forum].

## Contact us

<a href = "https://www.twitter.com/openflexure">Tweet Us</a>

<a href="mailto:contact@openflexure.org?subject=Enquiry from website">Email Us</a>  

[forum]: https://openflexure.discourse.group/

