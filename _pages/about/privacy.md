---
layout: page
title: Privacy Policy
permalink: /about/privacy
sitemap: false
redirect_from: 
    -   /downloads/privacy
---

To paraphrase "The World’s Greatest Privacy Policy":

We don't store your data, full stop.

We physically can't. We have nowhere to store it. We don't even have a server database to store it. So even if the government asked nicely to see your data, we wouldn't have anything to show them.

Data you take on your OpenFlexure devices is private. Browsing capture data using OpenFlexure Connect only moves copies of the data from your microscope to your client machine. No data, whatsoever, is sent to us.

All of our source code is openly available at [https://gitlab.com/openflexure](https://gitlab.com/openflexure). 