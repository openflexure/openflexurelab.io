---
layout: page
title: Pathology
permalink: /about/pathology
sitemap: true
---

![Daniel Rosen using our microscope for pathology education](/assets/about/rosen.jpg "Daniel Rosen using our microscope for pathology education"){: .image-medium .float-right}

<b> Microscopes are a key tool in pathology, used for diagnosing dozens of conditions including cancer and infections. Many areas of the Global South have limited access to lab-grade slide scanners, making diagnosis and training more challenging. </b>

As a low-cost automated microscope, the OpenFlexure Microscope can be used in low-resource settings, allowing clinicians to automate the scanning of their samples. This allows large area composite images to be produced automatically and viewed on a laptop, tablet or phone. The resulting composite images can be used for training, diagnosis or telepathology --- getting a second opinion from a remote specialist, without shipping the sample or delaying diagnosis. The composite images can be stored for better record keeping, saving a digital representation of the sample, not just the diagnosis.

Dr Daniel Rosen, Professor of Pathology at the Baylor College of Medicine, has been instrumental in demonstrating this use. As a global health specialist, he has used the OpenFlexure Microscope in Brazil and Rwanda, imaging patient samples to enable the training of medical students on digital images. We're working towards certification of our design, giving clinicians more confidence.

In January 2024, Daniel was joined by Dr Joe Knapper (OpenFlexure and University of Glasgow) and Dr Kelsey Hummels (MD Anderson Cancer Centre) to install four OpenFlexure Microscopes in the CHUB Hospital in Butare, southern Rwanda. The automated scanning of samples was assessed according to the College of American Pathologists' standards (publications in progress).

<b> Want to hear more about this work? Check out Dr Rosen's [OpenFlexureCon 2022 talk](/openflexurecon2022/community_stories/2022/10/20/openflexurecon-rosen) on the future of this project </b>

<b> Or to read the publications that have come from this work so far, Daniel has published about using the OFM for [esophageal biopsies](https://doi.org/10.1016/j.stlm.2024.100145) in Brazil. Cabello *et al.* have [evaluated our microscope](https://journals.plos.org/globalpublichealth/article?id=10.1371/journal.pgph.0002070) for diagnosing Tuberculosis and Rheumatic Heart Disease in Philippine health centres.

Below is an interactive image viewer for viewing automatically-stitched composite images from the OpenFlexure Micoroscope. 

<iframe
height= "400px"
width= "100%"
src= "https://johemianknapsody.github.io/viewer.html#"
>
</iframe>

[Return to index](/about/index)
