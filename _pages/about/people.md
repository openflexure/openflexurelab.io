---
layout: page
title: People
permalink: /about/people
sitemap: true
---
The OpenFlexure project was started by [Richard Bowman](https://www.gla.ac.uk/schools/physics/staff/richardbowman/) at the University of Cambridge. It is now developed and maintained by a global community, with contributions from academia, industry, healthcare and hobbyists. The current core team includes

![Richard Bowman demonstrating the OpenFlexure Microsope to IHI.](/assets/about/rwb_1.jpg "Richard Bowman demonstrating the OpenFlexure Microsope to IHI."){: .image-large .float-right}

* [Richard Bowman](https://www.gla.ac.uk/schools/physics/staff/richardbowman/)
* [Joe Knapper](https://www.gla.ac.uk/schools/physics/staff/josephknapper/)
* [Freya Whiteford](https://www.gla.ac.uk/schools/physics/staff/freyawhiteford/)
* [Julian Stirling](https://julianstirling.co.uk/)
* [William Wadsworth](https://researchportal.bath.ac.uk/en/persons/william-wadsworth)

The project has benefited from a number of former members, including:

* [Joel Collins](https://www.linkedin.com/in/joel-collins-b5415896/)
* [Kerrianne Harrington](https://researchportal.bath.ac.uk/en/persons/kerrianne-harrington)
* Kaspar Bumke  
* [Ed Meng](https://researchportal.bath.ac.uk/en/persons/ed-meng)
* [Samuel McDermott](https://cdt.sensors.cam.ac.uk/staff/dr-samuel-mcdermott)

<br>
<br>
<br>

The OpenFlexure project development has been a collaborative effort with our partners [Bongo Tech & Research Labs](https://btech.co.tz), [Ifakara Health Institute](http://ihi.or.tz/), the Texas Medical Centre, the [University of Cambridge](https://www.cam.ac.uk/), and [WaterScope](https://www.waterscope.org/). With particular thanks to:

![The Bongo Tech workshop.](/assets/about/DSC04475.jpg "The Bongo Tech workshop."){: .image-large .float-right}

![Bongo Tech logo](/assets/about/btech.png "Bongo Tech logo"){: .image-medium .no-shadow }

* Valerian Sanga
* Paul Nyakyi
* Grace Anyelwisye
* Stanley Mwalembe

![IHI Logo](/assets/about/IHI.png "IHI logo"){: .image-medium .no-shadow}

* Catherine Mkindi
* Valeriana Mayagaya
* Joram Mduda

![Collaborating with IHI.](/assets/about/meeting_IHI.jpg "Collaborating with IHI."){: .image-large .float-right}

![Texas Medical Center](/assets/about/tmc.jpeg "TMC logo"){: .image-medium .no-shadow}

* Daniel Rosen
* Kelsey Hummel

![University of Cambridge](/assets/about/Cambridge.svg "University of Cambridge"){: .image-medium .no-shadow}

* [Samuel McDermott](https://www.bss.phy.cam.ac.uk/directory/sjm263)
* Boyko Vodenicharski
* Filip Ayazi
* James Sharkey
* Darryl Foo
* Abhishek Ambekar
* [Fergus Riche](https://www.synbio.cam.ac.uk/directory/fr293)
* [Alexandre Kabla](http://www.eng.cam.ac.uk/profiles/ajk61)
* S Hortzmann
* [Pietro Cicuta](https://www.phy.cam.ac.uk/directory/cicuta)
* Y Chun
* [Jeremy J. Baumberg](https://www.phy.cam.ac.uk/directory/baumbergj)

![Demonstrating the OpenFlexure Microscope at GOSH 2018.](/assets/about/SDJB46002.jpg "Demonstrating the OpenFlexure Microscope at GOSH 2018."){: .image-large .float-right}

![WaterScope Logo](/assets/about/waterscope.png "WaterScope logo"){: .image-medium .no-shadow}

* Alex Patto
* Nalin Patel
* Tianheng Zhao
* Sammy Mahdi

![Although we've never all met in person, here's the team 'together'!](/assets/about/Combined_labeled.png "Although we've never all met in person, here's the team 'together'!"){:.image-x-large .float-center}
