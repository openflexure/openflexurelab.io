---
layout: page
title: OpenFlexure Vendors
permalink: /about/vendors
sitemap: false
---

This list of vendors is maintained for the OpenFlexure project by the [Open Science Shop](https://www.openscienceshop.org/). Please note that as an open source project you are not buying a product from OpenFlexure, but a product from these manufacturers based on our design.

<div class= "flex-container">
    <div class="flex-grow flex-container flex-container-vertical">
        {% for vendor in site.data.vendors %}
            <div class="flex-container flex-auto flex-valign-center elevated padding-small float-left margin-bottom-small">
                <div class="flex-grow flex-min-width-zero">
                    <a href="{{ vendor.website }}"><h2>{{ vendor.name }}</h2></a>
                    {% if vendor.about %}
                        <p> {{ vendor.about }}</p>
                    {% endif %}
                    <div class="flex-container flex-auto flex-valign-center flex-min-width-zero" style="width:100%">
                      {% if vendor.image != '' %}
                        <div class="flex-auto">
                          <img src="{{ vendor.image }}" alt="{{ vendor.name }}" class="image-large no-shadow">
                        </div>
                      {% endif %}
                      {% if vendor.logo != '' %}
                          <div class="flex-auto margin-left-small padding-small">
                              <a href="{{ vendor.website }}"><img src="{{ vendor.logo }}" alt="{{ vendor.business }}" class="image-s-m no-shadow"></a>
                          </div>
                      {% endif %}
                    </div>
                    <div class="vendor-details-container flex-valign-center flex-min-width-zero" style="width:100%">
                        <div class="flex-grow flex-min-width-zero">
                            <ul class="vendor-details">
                                <li class="home">
                                    <a href="{{ vendor.website }}">{{ vendor.website }}</a>
                                </li>
                                <li class="mail">
                                    <a href="mailto:{{ vendor.email }}">{{ vendor.email }}</a>
                                </li>
                                {% if vendor.location != '' %}
                                <li class="location">
                                    <span class="material-symbols-outlined"></span>
                                    {{ vendor.location }}
                                </li>
                                {% endif %}
                                {% if vendor.product_page != '' %}
                                <li class="product_page">
                                    <a href="{{ vendor.product_page }}">OpenFlexure product page</a>
                                </li>
                                {% endif %}
                                {% if vendor.shipping_terms != '' %}
                                <li class="shipping">
                                    {{vendor.shipping_terms}}
                                </li>
                                {% endif %}
                            </ul>
                        </div>
                        <div class="flex-grow">
                            {% if vendor.ofm_products.size > 0 %}
                                <h4>OpenFlexure Products</h4>
                                <ul>
                                    {% for product in vendor.ofm_products %}
                                    <li>{{product}}</li>
                                    {% endfor %}
                                </ul>
                            {% endif %}
                        </div>
                        <div class="contribution-icons-container">
                            {% if vendor.contrib_finance %}
                            <span class="material-symbols-outlined contribution-icon">credit_card_heart
                            <span class="tooltiptext">This vendor donates a proportion of sales to the OpenFlexure Project</span>
                            </span>
                            {% endif %}
                            {% if vendor.contrib_project %}
                            <span class="material-symbols-outlined contribution-icon">military_tech
                            <span class="tooltiptext">This vendor has made a significant contribution to the open source project.</span>
                            </span>
                            {% endif %}
                            {% if vendor.contrib_sanga %}
                            <span class="material-symbols-outlined contribution-icon">credit_card_gear
                            <span class="tooltiptext">This vendor donates a proportion of Sangaboard sales to the Sangaboard project founder, or buys boards from a manufacturer who does.</span>
                            </span>
                            {% endif %}
                            {% if vendor.is_sanga %}
                            <span class="material-symbols-outlined contribution-icon">engineering
                            <span class="tooltiptext">This vendor is the Sangaboard project founder!</span>
                            </span>
                            {% endif %}
                        </div>
                    </div>
                </div>
            </div>
        {% endfor %}
    </div>
</div>
