---
layout: page
title: Media and Publications
permalink: /about/media-publications
sitemap: true
---
## Publications and conferences

The work of the OpenFlexure project has been published in scientific journals and presented at conferences. If you use our work, please consider citing us!

| Title | Year | Journal / Conference |
| --- | --- | ---: |
| [Developing the OpenFlexure Microscope towards medical use: technical and social challenges of developing globally accessible hardware for healthcare](https://doi.org/10.1098/rsta.2023.0257) | 2024 | Philosophical Transactions of the Royal Society A |
| [Utility of a low-cost 3-D printed microscope for evaluating esophageal biopsies](https://doi.org/10.1016/j.stlm.2024.100145) | 2024 | Annals of 3D Printed Medicine |
| [Controlling and scripting laboratory hardware with open-source, intuitive interfaces: OpenFlexure Voice Control and OpenFlexure Blockly](https://doi.org/10.1098/rsos.221236) <br/> [arXiv](https://doi.org/10.48550/arXiv.2209.14947) | 2023 | Royal Society Open Science |
| [Multi-modal microscopy imaging with the OpenFlexure Delta Stage](https://doi.org/10.1364/OE.450211) <br/> [arXiv](https://doi.org/10.48550/arXiv.2112.05804) | 2022 | Optics Express |
| [Simplifying the OpenFlexure Microscope software with the web of things](https://doi.org/10.1098/rsos.211158) | 2021 | Royal Society Open Science |
| [Fast, high precision autofocus on a motorised microscope: automating blood sample imaging on the OpenFlexure Microscope](https://doi.org/10.1111/jmi.13064) | 2021 | Journal of Microscopy|
| [HardOps: Utilising the software development toolchain for hardware design](https://www.techrxiv.org/articles/preprint/HardOps_Utilising_the_software_development_toolchain_for_hardware_design/15052848) | 2021 | TechRxiv |
| [An Open‐Source Modular Framework for Automated Pipetting and Imaging Applications](https://doi.org/10.1002/adbi.202101063) | 2021 | Advanced Biology |
| [Transitioning from Academic Innovation to Viable Humanitarian Technology: The Next Steps for the OpenFlexure Project](https://ieeexplore.ieee.org/abstract/document/9576953) | 2021 | IST-Africa conference |
| [Low-cost, multimodal bioimaging with the OpenFlexure Delta Stage Microscope](https://www.youtube.com/watch?v=RMnBPtE2gu0) | 2020 | RMS Frontiers in Bioimaging |
| [The OpenFlexure Project. The technical challenges of Co-Developing a microscope in the UK and Tanzania](https://ieeexplore.ieee.org/document/9342860) <br/> [AfricaArXiv](https://doi.org/10.5281/zenodo.3862777) | 2020 | IEEE Global Humanitarian Technology Conference (GHTC) |
| [Robotic microscopy for everyone: the OpenFlexure Microscope](https://doi.org/10.1364/BOE.385729) <br/>  [bioRxiv preprint](https://doi.org/10.1101/861856) **[Best paper prize](https://opg.optica.org/boe/fulltext.cfm?uri=boe-15-2-1148&id=545956) | 2020 | Biomedical Optics Express
| [The OpenFlexure Block Stage: sub-100 nm fibre alignment with a monolithic plastic flexure stage](https://doi.org/10.1364/OE.384207) | 2020 | Optics Express |
| [Flat-Field and Colour Correction for the Raspberry Pi Camera Module](http://doi.org/10.5334/joh.20)  | 2020 | Journal of Open Hardware |
| [A one-piece 3D printed flexure translation stage for open-source microscopy](https://dx.doi.org/10.1063/1.4941068)| 2016 | Review of Scientific Instruments

## Citing publications

The OpenFlexure project is now used and cited by a growing number of academic publications.  If you have used the designs in your work, please let us know and we'll add you to the list below.

* P. Thongtade and W. Pora, [Development of an Autoscan Motorized Microscope Utilizing the OpenFlexure Architecture](https://doi.org/10.1109/ECTI-CON60892.2024.10594841) ECTI-CON, Thailand, (2024)
* Sven Schulze, Kumar Arumugam, Stephan Schlamminger, Ryan Fitzgerald, R Michael Verkouteren, René Theska and Gordon Shaw, [Development of a high precision electrostatic force balance for measuring quantity of dispensed fluid as a new calibration standard for the becquerel](https://doi.org/10.1088/1361-6501/ad3a06), Meas. Sci. Technol. **35** 085020 (2024)
* Koki Uebo, Yuto Shiokawa, Ryunosuke Takahashi, Suguru Nakata and Hiroki Wadati [Development of a magneto-optical Kerr microscope using a 3D printer](https://doi.org/10.12688/f1000research.133292.2) [version 2; peer review: 1 approved, 1 not approved]. F1000Research 2024, 12:860 
* Mark Kristan Espejo Cabello and Jeremie E. De Guzman [Utilization of accessible resources in the fabrication of an affordable, portable, high-resolution, 3D printed, digital microscope for Philippine diagnostic applications](https://doi.org/10.1371/journal.pgph.0002070), PLoS Global Public Health **3**(11): e0002070 (2023)
* A. Reguilon, W. Bethard and E. Brekke [A low-cost confocal microscope for the undergraduate lab](https://doi.org/10.1119/5.0128277), American Journal Physics **91**(5): 404 (2023)
* Tai The Diep, Sarah Helen Needs, Samuel Bizley, and Alexander D. Edwards [Rapid Bacterial Motility Monitoring Using Inexpensive 3D-Printed OpenFlexure Microscopy Allows Microfluidic Antibiotic Susceptibility Testing](https://doi.org/10.3390/mi13111974) Micromachines **13**, no. 11: 1974 (2022)
* T. Matsui and D. Fujiwara [Optical sectioning robotic microscopy for everyone: the structured illumination microscope with the OpenFlexure stages](https://doi.org/10.1364/OE.461910), Opt. Express **30**(13), pp. 23208-23216 (2022).
* Bowen Chen, Max Nadeau, Ming Y. Lu, Jana Lipkova, and Faisal Mahmood [Real Time, Point-of-Care Pathology Diagnosis via Embedded Deep Learning](https://digitalpathologyassociation.org/_data/media/391/8chen_poster.pdf), a poster awarded the "best research" prize at [Pathology Visions 2020](https://doi.org/10.4103/2153-3539.326643)
* Stephen D. Grant, Kyle Richford, Heidi L. Burdett, David McKee and Brian R. Patton [Low-cost, open-access quantitative phase imaging of algal cells using the transport of intensity equation](https://doi.org/10.1098/rsos.191921) Royal Society Open Science **7**, 1 (2020)
* Grant SD, Cairns GS, Wistuba J and Patton BR. [Adapting the 3D-printed Openflexure microscope enables computational super-resolution imaging](https://doi.org/10.12688/f1000research.21294.1) [version 1; peer review: 2 approved]. F1000Research 2019, 8:2003


## Media

Some of the articles that the OpenFlexure project have featured in. If you want to write about the OpenFlexure project then please [let us know](/about/need-help)!

| Title | Date | Source |
|---|---|---:|
| [A $500 Open Source Tool Lets Anyone Hack Computer Chips With Lasers](https://www.wired.com/story/rayv-lite-laser-chip-hacking-tool/) | August 2024 | Wired.com |
| [Telepathology for Everybody: Part 2 - Meet the pathologists bringing telepathology to low-resource settings](https://thepathologist.com/inside-the-lab/telepathology-for-everybody-part-2)|April 2024|The Pathologist|
| [Telepathology for Everybody: Part 1 - The story of the OpenFlexure 3D-printable microscope](https://thepathologist.com/diagnostics/telepathology-for-everybody-part-1)|April 2024|The Pathologist|
| [UVA Professors, Students Launch Open-Source Maker Library for Teachers](https://education.virginia.edu/news-stories/uva-professors-students-launch-open-source-maker-library-teachers)|March 2024|University of Virginia|
| [Sandra Waterwood Acquah: Navigating the World of Synthetic Biology Through iGEM](https://blog.igem.org/blog/sandra-waterwood-acquah-navigating-the-world-of-synthetic-biology-through-igem)|September 2023|iGEM Community|
| [Microscopy Workshops in Ghana](https://www.mdc-berlin.de/news/news/microscopy-workshops-ghana)|March 2023|Max Delbrück Center|
| [3D printed microscope promises bigger, better science](https://azimpremjiuniversity.edu.in/microscopy-facility/3d-printed-microscope-promises-bigger-better-science)|March 2023|Azim Premji University|
| [WACCBIP HOLDS BIO-IMAGING WEST AFRICA 2022 WORKSHOP](https://www.waccbip.org/news-events/news/waccbip-holds-bio-imaging-west-africa-2022-workshop)|October 2022|West African Centre for Cell Biology of Infectious Pathogens (WACCBIP)|
| [Microscopy made to order](https://www.nature.com/articles/s41592-021-01313-1) | November 2021 | Nature Methods |
| [Distributed Manufacturing of Open Hardware: A Report of the Open Hardware Distribution & Documentation Working Group](https://www.law.nyu.edu/centers/engelberg/news/2021-11-02-distributed-manufacturing) | November 2021 | Engleberg center on Innovation Law & Policy |
| [High-spec open-source microscopy for all](https://physicsworld.com/a/high-spec-open-source-microscopy-for-all/) | November 2021 | Physics World |
| [The impact of Open Source Software and Hardware on technological independence, competitiveness and innovation in the EU economy](https://digital-strategy.ec.europa.eu/en/library/study-about-impact-open-source-software-and-hardware-technological-independence-competitiveness-and) | May 2021 | European Commission |
| [OpenFlexure: an open-source 3D printed microscope](https://focalplane.biologists.com/2021/10/19/openflexure-an-open-source-3d-printed-microscope/) | October 2021 | FocalPlane |
| [Making hardware 'open source' can help us fight future pandemics - here's how we get there](https://theconversation.com/making-hardware-open-source-can-help-us-fight-future-pandemics-heres-how-we-get-there-153280) | January 2021 | The Conversation |
| [How DIY technologies are democratising science](https://doi.org/10.1038/d41586-020-03193-5) | November 2020 | Nature technology deature |
| [Building Blocks for Better Science: Case Studies in Low-Cost and Open Tools for Science](https://www.wilsoncenter.org/sites/default/files/media/uploads/documents/STIP%20THING%20Tank%20Building%20Blocks%20for%20Better%20Science.pdf) | November 2020 | Wilson Center |
| [OpenFlexure Microscope](https://magpi.raspberrypi.org/issues/96) | August 2020 | MagPi |
| [Focusing on a brighter future: How an ultra-low-cost 3D printed microscope could revolutionise healthcare in LMICs](http://www.bsac.org.uk/focusing-on-a-brighter-future-how-an-ultra-low-cost-3d-printed-microscope-could-revolutionise-healthcare-in-lmics/) | June 2020 | British Society for Antimicrobial Chemotherapy |
| [Print your own laboratory-grade microscope for US$18](https://www.sciencedaily.com/releases/2020/05/200504101627.htm) | May 2020 | Science Daily |
| [Print your own laboratory-grade microscope for US$18](https://phys.org/news/2020-05-laboratory-grade-microscope-us18.html) | May 2020 | Phys.org |
| [Print your own laboratory-grade microscope for £15](https://www.bath.ac.uk/announcements/print-your-own-laboratory-grade-microscope-for-15/) | April 2020 | University of Bath |
| [Robotic microscopy for everyone: the OpenFlexure Microscope](https://prelights.biologists.com/highlights/robotic-microscopy-for-everyone-the-openflexure-microscope/) | March 2020 | preLights |
| [Precision for $100: The OpenFlexure Microscope](https://www.labonthecheap.com/precision-for-100-the-openflexure-microscope/) | August 2019 | Lab on the Cheap |
| [OpenFlexure Block Stage: 3D printed sub-micron mechanical precision](https://www.labonthecheap.com/openflexure-block-stage-3d-printed-sub-micron-mechanical-precision/) | June 2019 | Lab on the Cheap |
| ["Sustainable Development Technology and Beyond" -- VigyanShaala @ UPES, Dehradun, Uttarakhand, India](https://medium.com/@vigyanshaala/sustainable-development-technology-and-beyond-vigyanshaala-upes-8-01-2019-f1c8262245a1) | March 2019 | Medium |
| [Five innovative ways to use 3D printing in the laboratory](https://www.nature.com/articles/d41586-018-07853-5) | January 2019 | Nature toolbox |
| [Could 3D printed microscopes improve water testing?](https://www.theguardian.com/business-to-business/2018/nov/29/could-3d-printed-microscopes-improve-water-testing) | December 2018 | The Guardian |
| [Open-science hardware in the developing world](https://physicsworld.com/a/open-science-hardware-in%E2%80%AFthe-developing-world/) | August 2018 | Physics world |
| [6 cool open-source projects built in Bristol and Bath](https://www.techspark.co/2018/02/12/6-cool-open-source-projects-built-in-bristol-and-bath/) | February 2018 | TechSPARK |
| [The quest for open science](http://www.techradar.com/news/the-quest-for-open-science) | November 2017 | TechRadar |
| [The OpenFlexure Microscope goes to MozFest 2017](https://medium.com/@richardbowman_52325/the-openflexure-microscope-goes-to-mozfest-2017-b27499ec896a) | October 2017 | Medium |
| [3D printed microscopes to boost science in developing countries](https://www.bath.ac.uk/announcements/3d-printed-microscopes-to-boost-science-in-developing-countries/) | August 2017 | University of Bath |
| [The OpenFlexure Microscope: A Lifesaving Water-Testing Device You Can 3D Print at Home](https://3dprint.com/165457/openflexure-3d-printed-microscope/) | February 2017 | 3DPRINT.COM |
| [Open source, 3D printed microscope runs on Raspberry Pi](http://linuxgizmos.com/open-source-3d-printed-microscope-runs-on-raspberry-pi/) | January 2017 | LinuxGizmos.com |
| [This open source, 3D printed microscope runs on Raspberry Pi](https://blog.adafruit.com/2017/01/20/this-open-source-3d-printed-microscope-runs-on-raspberry-pi-piday-raspberrypi-raspberry_pi/) | January 2017 | Adafruit blog |
| [This 3D-Printed Microscope Is Bringing Water Testing to the Developing World](https://blog.hackster.io/a-3d-printed-raspberry-pi-based-microscope-for-water-testing-2423f5d0e6a5) | January 2017 | Hackster.io |
| [Female Scientists Journey to Antarctica and into Leadership](https://chuffed.org/project/female-scientists-journey-to-antarctica-and-into-leadership) | March 2016 | chuffed.org |




