---
layout: page
title: About
permalink: /about/
sitemap: true
---

The OpenFlexure project uses 3D printers and off the shelf components to build open-source, lab-grade microscopes for a fraction of traditional prices. Used in over 50 countries and every continent, the project aims to enable [Microscopy for Everyone].

Once based primarily at the University of Bath and University of Cambridge, the project has spread. From the Antarctic ice to pathology labs in Rwanda, OpenFlexure Microscopes are transforming the role of microscopy in healthcare, education and in the field. Conversations, suggestions and issues are all hosted on our [forum].

As an academic project, the [core development team] is now based at the University of Glasgow. We're grateful for the support we've received from our [funders], as well as our collaborations with groups including the Baylor College of Medicine, Bongo Tech & Research Labs, and Mboalab.

We always love hearing from people with suggestions, questions or ideas for collaboration. The easiest place is on our [forum], or you can [email us] for anything you're not ready to share .


## Some of our highlighted projects
{: #examples}

### Click an image to hear the story


[![Joram at IHI using the OpenFlexure Microscope to image blood smears.](/assets/about/malaria_story.jpg "Joram at IHI using the OpenFlexure Microscope to image blood smears."){: .highlight-image }][malaria]
[![Daniel Rosen using our microscope for pathology education](/assets/about/pathology_story.jpg "Daniel Rosen using our microscope for pathology education"){: .highlight-image}][pathology]
[![An AfricaOSH workshop building OFMs](/assets/about/workshops_story.jpg "An AfricaOSH workshop building OFMs"){: .highlight-image} ][workshops]
[![Soil health microscopy with OpenFlexure](/assets/about/agriculture_story.jpg "Soil health microscopy with OpenFlexure"){: .highlight-image}][agriculture]

<div class='embedded-youtube'><iframe src='https://www.youtube.com/embed/sRHeQRbf2U8' frameborder='0' allowfullscreen></iframe></div>


<div class="flex-container-tight">
{% for navcategory in site.data.navigation %}
{% if navcategory.title == 'About' %}
{% for aboutitems in navcategory.sublinks offset:1 %}
{% assign index = index | plus: 1 %}
<div>
<a class="download-btn outline width-s-m" href="{{aboutitems.url}}">
{{aboutitems.title}}
</a>
</div>
{% if index == 3 %}
{% assign index = 0 %}
</div>
<div class="flex-container-tight">
{% endif %}
{% endfor %}
{% endif %}
{% endfor %}
</div>


[core development team]: /about/people/
[forum]: https://openflexure.discourse.group/
[funders]: /about/funding/
[Microscopy for Everyone]: https://opg.optica.org/boe/fulltext.cfm?uri=boe-11-5-2447&id=429869
[email us]: mailto:contact@openflexure.org?subject=Enquiry%20from%20website
[malaria]: /about/malaria
[pathology]: /about/pathology
[workshops]: /about/workshops
[agriculture]: /about/agriculture
