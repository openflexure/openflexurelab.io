---
layout: page
title: Medical Certification FAQ
permalink: /about/medical-devices
sitemap: true
---
# Medical Certification FAQ

> Disclaimer: Following an open design, however good it is, does not on its own meet the requirements for a medical device.  It not safe to make your own medical devices.  Diagnosis must be performed by healthcare professionals with the relevant training and qualifications as set out in local law.  Diagnostic devices must be produced by people and organisations who are competent to do so, and who are able to accept liability for any devices they produce under their local legal system.  

The OpenFlexure Microscope is a general purpose laboratory instrument, with uses in a wide range of research, educational, and medical settings.   We are passionate about making high quality digital diagnostics more accessible. This page describes our current approach to enable manufacturers to create medically certified microscopes from the OpenFlexure design.  The journey from working prototype to certified product is a long one, and we have attempted to explain the regulatory landscape and our proposed approach below.

## Are microscopes medical devices?

Microscopes like the OpenFlexure Microscope are routinely used in clinical laboratories around the world to analyse tissues, cells, and bodily fluids for conditions ranging from parasites to cancer.  In most frameworks, they are classed as in-vitro diagnostic (IVD) devices rather than medical devices. In-vitro diagnostic devices are a related category of device that do not directly affect a patient, but are used to guide their treatment.  As such, they are subject to very similar regulations and processes.  Most microscopes are considered general purpose laboratory equipment, meaning that they are not sold for the diagnosis of a particular condition. They are used by qualified laboratory technicians as part of a diagnostic procedure rather than simply taking a sample and returning a diagnosis.

## Medical device certification

In more or less every jurisdiction in the world, medical devices are strictly regulated to ensure patient safety.  This regulation encompasses every aspect of a product including its design, manufacturing, quality control, monitoring, and disposal.  Consequently, **it is not possible to certify a design as a medical device** because this does not provide any assurance that it will be assembled competently.  Medical device certification is issued to a particular manufacturer for a particular product. Regulators assess that the product is designed competently and performs adequately when built correctly. They also assess that the manufacturer has the processes in place to ensure that any device supplied has been made to the required standard.

Quality management processes according to ISO13485 and risk management according to ISO14971 are almost universally required before a company may submit devices for assessment and eventual certification.  This requires rigorous documentation of how the organisation will ensure that products are of consistently high quality and that any risk to patient safety is minimised and kept below acceptable levels.  Achieving this quality management accreditation is a significant undertaking in itself.  While we are keen to support local production of diagnostic technology by small and less well funded companies, we also recognise that quality management is important in medical device production.  Our mission is not to change the way medical devices are regulated. We want to share as much documentation and know-how as we can, to enable more people and organisations to work towards equity in diagnostics.

## What are we doing?

The OpenFlexure Project aims to share as much as possible of the documentation to support bringing the microscope to market an in-vitro diagnostic device, based on the OpenFlexure Microscope design.  We also aim to make the OpenFlexure Microscope the best possible starting point for an IVD product.  These aims lead to a number of activities, most of which have been supported by funding from EPSRC, GCRF, and the Royal Society:

* Design for quality, reliability, and manufacturability.  Version 7 of the microscope has focused on making it ready for use inside and outside the research laboratory.  This involves:
  - Enclosing all electronic parts and cables for robustness.
  - Eliminating common points of mechanical failure.
  - Ensuring that the design is as maintainable as possible.
* Document the microscope design to support manufacturers that wish to use the design as the basis for a medical device:
  - Split the documentation into clear assembly instructions, design rationale, and guidance for use, maintenance, and repair.
  - Document design decisions and review of changes with a view to aligning with ISO13485.
  - Add quality checks to the assembly instructions, eventually including interactive documents that produce auditable reports.
* Adopt a risk-based approach to development priorities
  - Conduct hazard identification exercises.
  - Maintain a register of risks, and feed back risk mitigation measures into future development.

## Where's the company?

At present, there is no single legal entity that is responsible for the design of the OpenFlexure Microscope.  In the future, we hope to find or set up a foundation to take ownership of the design.  We are working closely with Bongo Tech & Research Labs in Dar es Salaam, Tanzania, to ensure that the OpenFlexure Microscope design could form the basis of a product there.  Our aim is that they will act as the legal manufacturer in Tanzania, rather than being subservient to a UK organisation.  Manufacturers of the OpenFlexure Microscope may choose to have a contract with the foundation managing our design once it exists.  We anticipate this will make it easier to show regulators that a company has the necessary access to designers intimately familiar with the microscope. This will allow them to ensure the design is adequately maintained and improved in response to medical device monitoring procedures.

## How can I help?

There are many ways you can contribute to this open-source effort.  We have tried to detail them on our [contributing] page. Specifically in relation to medical device certification, people who can help improve the documentation (both assembly instructions and the other documentation around the project) are extremely welcome.  If you have medical device experience, or you represent an organisation willing to help support an OpenFlexure Foundation, we would be delighted to hear from you.
