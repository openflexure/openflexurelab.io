---
layout: page
title: OpenFlexure Connect
permalink: /software/openflexure-connect
sitemap: true
---

## Installation

{% include software/install-openflexure-connect.md %}

## Guide to the interface

<div><a class="download-btn outline width-s-m" href="https://openflexure-microscope-software.readthedocs.io/en/latest/webapp/index.html">Using the interface</a></div>

## Source

Source code for OpenFlexure Connect is available on [GitLab](https://gitlab.com/openflexure/openflexure-connect).

## Legacy versions

Legacy versions of OpenFlexure Connect (previously known as OpenFlexure eV) can be found on the [build server](https://build.openflexure.org/openflexure-ev/).

## Legal

OpenFlexure Connect is licensed under the [GNU General Public License (v3)](https://choosealicense.com/licenses/gpl-3.0/).
Installed our software? See our [privacy policy](/about/privacy).