---
layout: page
title: Software
permalink: /software/
sitemap: true
redirect_from: /downloads/
---

The OpenFlexure devices use an embedded Raspberry Pi to control the hardware.  They can be run either as a stand-alone device, by plugging in a monitor, keyboard, and mouse, or controlled over the network.  In either case, the simplest way to [set up the Raspberry Pi] is to download the pre-built SD card image that contains a full operating system with all the necessary software pre-installed.

Our graphical interface can be viewed through [OpenFlexure Connect], running either on the Raspberry Pi (where it is preinstalled) or on another computer on the same network.  The interface can also be displayed in a web browser.

Scripting the microscope is possible via our [programming clients] that provide simple APIs in Python and MATLAB to control the microscope.

Finally, the hardware control code on the Raspberry Pi is implemented as a Python application with an HTTP API.  It has a mechanism to add [extensions] that run on the server, and also displays interactive [API documentation] allowing you to control it from any environment that can send HTTP requests.

The software architecture is described in much more detail in our [preprint], soon to be published.

![An overview of the software architecture used for the OpenFlexure Microscope.](/assets/software/software_architecture.svg){: .image-x-large .float-center}

[OpenFlexure Connect]: /software/openflexure-connect
[programming clients]: /software/programming-clients
[extensions]: /software/extensions
[API documentation]: https://openflexure-microscope-software.readthedocs.io/en/master/api.html
[set up the Raspberry Pi]: /projects/microscope/install
[preprint]: https://arxiv.org/abs/2101.00933