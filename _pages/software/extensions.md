---
layout: page
title: Software Extensions
permalink: /software/extensions
sitemap: true
---
Extensions allow functionality to be added to the OpenFlexure Microscope without having to modify the base code. They have full access to the microscope hardware, including direct access to the attached camera and stage objects.

Additionally, extensions are able to define user interface forms that will be automatically added to OpenFlexure Connect, once connected to the microscope.

## Finding extensions

Several extensions have been developed for the microscope and can be found in the [microscope-extensions GitLab repository](https://gitlab.com/openflexure/microscope-extensions). Currently, they each have their own instructions for installation.

## Developing extensions

Developer documentation for the microscope server software can be found on [Read the Docs](https://openflexure-microscope-software.readthedocs.io/en/stable/). This includes documentation and tutorials on [developing extensions for the microscope software](https://openflexure-microscope-software.readthedocs.io/en/stable/plugins.html). There is useful information for setting up a developer environment for developing extensions in [the handbook](https://openflexure.gitlab.io/microscope-handbook/develop-extensions/index.html#).

Please see our [contribution guidelines](/contribute) for more information on contributing back to the project. 
