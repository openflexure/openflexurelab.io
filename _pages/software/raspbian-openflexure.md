---
layout: page
title: Raspbian-OpenFlexure
permalink: /software/raspbian-openflexure
sitemap: true
---

{% include software/install-raspbian.md %}

## Managing the microscope server

Our installer includes various commands to simplify common tasks when using the OpenFlexure Microscope.
From the terminal on your Raspberry Pi, you will have available commands such as:

`ofm start` - Start the server background service

`ofm stop` - Stop the server background service

`ofm update` - Pull and install the latest version of the OFM CLI itself

`ofm upgrade` - Pull and install the latest version of the server

For a complete list of available commands, run `ofm help`

## Raspberry Pi configuration

Additional Raspberry Pi configuration options, such as network and interface settings, can be accessed by running `sudo raspi-config` from your terminal.

## Source

Source code for the OpenFlexure Microscope server is available on [GitLab](https://gitlab.com/openflexure/openflexure-microscope-server).

## Legacy versions

Previous versions of the OpenFlexure Microscope server can be found on the [build server](https://build.openflexure.org/openflexure-microscope-server/).
Previous versions of Raspbian-OpenFlexure can be found on the [build server](https://build.openflexure.org/raspbian-openflexure/).

## Legal

The OpenFlexure Microscope server is licensed under the [GNU General Public License (v3)](https://choosealicense.com/licenses/gpl-3.0/).
Installed our software? See our [privacy policy](/about/privacy).
