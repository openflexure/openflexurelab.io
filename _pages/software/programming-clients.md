---
layout: page
title: Programming Clients
permalink: /software/programming-clients
sitemap: true
---

## Python Library

If you want to write your own scripts to perform particular experiments or protocols, it's useful not to have to package them up as extensions.
Our Python library makes it easy to control your microscope from a simple Python script that can run either on the Raspberry Pi, or over the network.

This can be particularly useful running from a Jupyter notebook on a laptop, allowing you to plot graphs and display images as you go.

The library can be installed with `pip install openflexure-microscope-client`.

Source and documentation can be found on the [GitLab repository](https://gitlab.com/openflexure/openflexure-microscope-pyclient).

## MATLAB Client

The MATLAB Client allows you to connect and control an OpenFlexure Microscope over a network using MATLAB.  You are able to move the microscope, get images, use a live preview, and run extensions.

The files can be downloaded from [![View OpenFlexure Microscope MATLAB Client on File Exchange](https://www.mathworks.com/matlabcentral/images/matlab-file-exchange.svg)](https://uk.mathworks.com/matlabcentral/fileexchange/86478-openflexure-microscope-matlab-client).

Source and documentation can be found on the [GitLab repository](https://gitlab.com/openflexure/openflexure-microscope-matlab-client)

## OpenFlexure Blockly

OpenFlexure Blockly is a beginner friendly interface for creating scripts to control the OpenFlexure Microscope.  Based on Google's Blockly visual programming language, you can drag-and-drop 'blocks' to create control logic and send commands to the OpenFlexure Microscope.  

You can try it out without installation on the [OpenFlexure Blockly webapp](http://openflexure.gitlab.io/openflexure-blockly/). 

For offline access, you can [download it onto your computer](https://gitlab.com/openflexure/openflexure-blockly#on-your-computer), or [download it onto your microscope](https://gitlab.com/openflexure/openflexure-blockly#on-your-microscope) for a microscope extension.

Source and documentation can be found on the [GitLab repository](https://gitlab.com/openflexure/openflexure-blockly).  

Our paper describing our design decisions for OpenFlexure Blockly has been publised in [Royal Society Open Science](https://doi.org/10.1098/rsos.221236).
