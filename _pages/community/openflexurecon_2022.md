---
layout: page
title: OpenFlexureCon 2022
permalink: /events/openflexurecon22
sitemap: true
image: /assets/openflexurecon2022/group_photo.jpg
---

We hosted the first in-person gathering of the OpenFlexure community in Bath, in July 2022.  It was a three day gathering for people from all over the world to showcase what they were doing, build connections, and help set future directions for the project. It was held at the [Bath Royal Literary and Scientific Institution](https://www.brlsi.org/), and there are now notes, articles, and recordings available below.

![Group photo at OpenFlexureCon 2022](/assets/openflexurecon2022/group_photo.jpg)

## Articles on the event

With many thanks to Julieta Arancio for preparing these, we have write-ups of several of the projects that were represented.  We are very keen to document more of the exciting things happening with the OpenFlexure Community, so if you'd like to add to this list (whether or not you were at OpenFlexureCon), please get in touch on the forum or by email.

A [recording of Richard's introduction] is available on YouTube:

[recording of Richard's introduction]: https://youtu.be/Chg3wucPVwM "Recording of Richard Bowman's introduction to the OpenFlexure Project."

<div class="flex-grow flex-container flex-container-vertical margin-left-very-small">
{% for post in site.posts %}
{% if post.categories contains "openflexurecon2022" %}
{% include homecard.html link=post.url title=post.title content=post.tldr hero=post.image %}
{% endif %}
{% endfor %}
</div>

## Recordings and notes

We've uploaded lots of notes from the event to the [forum category].  Recordings of the talks from Tuesday are in a [YouTube playlist].  Many thanks to the Royal Academy of engineering, who enabled us to engage the excellent [paraphrase studio] to live-scribe the event.

[paraphrase studio]: https://www.paraphrase.studio/

![Unconference live-scribe illustration](/assets/openflexurecon2022/OpenFlexureCon2022_unconference.jpg)


## Aims and format

We met over two half-days and one full day, with some short talks and lots of room for discussion.  There were be posters and hardware demos on the evening of Monday 11th, and plenty delegates brought microscopes and other creations for the in-person show and tell that we have all been missing these last few years.  There was lots of discussion in small and large groups, most of which has been summarised in the [forum category].

![Timeline and future directions](/assets/openflexurecon2022/OpenFlexureCon2022_timeline_and_future.jpg)

This was an in-person meeting, with some pre-recorded talks from delegates who could not attend.  Recordings are now available in a [YouTube playlist], and notes from the discussions are in the [forum category].

![GitBuilding workshop live-scribed notes](/assets/openflexurecon2022/OpenFlexureCon2022_gitbuilding.jpg)

## Programme

### Monday 11th July 2022

* 12 noon - 1pm: *Arrivals and lunch*
* **1:00 - 1:15pm**: Welcome and introduction
* **1:15 - 1:45**: Icebreaker
* **1:45 - 2:00pm**: Introduction to unconference format and discussion topics
* **2:00 - 2:30pm**: Unconference planning
* 2:30 - 3:00pm: *Coffee*
* **3:00 - 3:45pm**: Unconference session 1
* 3:45-4:00pm: *Changeover and short break*
* **4:00 - 4:45pm**: Unconference session 2
* **4:45 - 5:15pm**: Wrap up
* **5:15 - 9pm**: Drinks, nibbles, demos, posters, and discussions

### Tuesday 12th July 2022

* 9:00 - 9:30am: *Coffee and arrival*
* **9:30 - 10:00am**: Welcome and introduction to the OpenFlexure project ([recording](https://www.youtube.com/watch?v=Chg3wucPVwM&list=PL54P4SoUNm5nHO83RpAROxBZ2wlDCl2-q&index=3))
* **10:00 - 10:30am**: Short talks
    - Paul Nyakyi, Bongo Tech & Research Labs, Tanzania
    - Daniel Rosen, Baylor College of Medicine, USA ([recording](https://www.youtube.com/watch?v=kquKK2eL4tU&list=PL54P4SoUNm5nHO83RpAROxBZ2wlDCl2-q&index=5))
    - Joram Mduda, Ifakara Health Institute, Tanzania ([video](https://www.youtube.com/watch?v=joDKMM3xalY&list=PL54P4SoUNm5nHO83RpAROxBZ2wlDCl2-q&index=2))
* **10:30 - 11:00am**: Panel Discussion
* 11:00 - 11:30am: *Coffee*
* **11:30 - 12:00pm**: Short talks
    - Niamh Burke, University College Dublin ([recording](https://www.youtube.com/watch?v=t2t2Q7hI5wY&list=PL54P4SoUNm5nHO83RpAROxBZ2wlDCl2-q&index=4))
    - Tatsunosuke Matsui, Mie University, Japan ([video](https://www.youtube.com/watch?v=YjLOR5VbISg&list=PL54P4SoUNm5nHO83RpAROxBZ2wlDCl2-q&index=1))
    - Oliver Higgins, University of Glasgow, UK
    - Stephane Fadanka, Mboalab, Cameroon
* **12:00 - 12:30pm**: Panel Discussion
* 12:30 - 1:30pm: *Lunch*
* **1:30 - 2:00pm**: Short talks
    - Fernando Castro, reGOSH - Ayllu Cooperative, Argentina
    - Samuel McDermott, University of Cambridge
    - Benedict Diederich, Universitat Jena, Germany
    - Joe Knapper, University of Bath, UK
* **2:00 - 2:30pm**: Panel discussion
* 2:30 - 3:00pm: *Coffee*
* **3:00 - 3:15pm**: What next for OpenFlexure?
* **3:15 - 4:00**: Table discussions with topics including:
    - Adding features for ready-made microscopy
    - Modularity and linking up projects
    - Commercialisation and scaling up
    - Medical use and certification
* 4:00 - 4:05: *Move tables (and top up coffee)*
* **4:05 - 4:50pm**: Table discussions 2
* **4:50 - 5:00pm**: Wrap-up
* **7:00 - 9:00pm**: Conference dinner

### Wednesday 13th July 2022

* 9:00 - 9:30am: *Coffee and arrival*
* **9:30 - 10:15am**: Unconference session 3 (including thinking about project pitches)
* **10:15 - 10:45am**: Plenary discussion and project pitches
* 10:15 - 11:15am: *Coffee*
* **11:15 - 11:50am**: Group work on next steps
* **11:50 - 12:00pm**: Wrap-up and departure


[forum category]: https://openflexure.discourse.group/c/openflexurecon22/11
[YouTube playlist]: https://www.youtube.com/playlist?list=PL54P4SoUNm5nHO83RpAROxBZ2wlDCl2-q