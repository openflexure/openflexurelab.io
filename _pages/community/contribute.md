---
layout: page
title: Contribute
permalink: /contribute
sitemap: true
---

We welcome all community contributions across all OpenFlexure projects. Full source code is available for all hardware and software available on our [**GitLab project pages**](https://gitlab.com/openflexure/).

It may be worth starting a conversation with the developers before getting stuck into big code changes. You can either do this through an issue on the repository, or on our [community forum](https://openflexure.discourse.group).


## Submitting Code

To submit code to an OpenFlexure repository you can follow these steps:

1. Fork the repository.
1. If you are unsure which branch to use start by working on `master` on your fork. You can also make your own feature branch if you prefer.
1. When you are ready open a merge request to merge into the OpenFlexure `master` branch.

## Our Internal Development

For our internal development we have a similar workflow:

* `master` or `main` is always the latest working version.
* New features and bug fixes are developed on branches.
* Merge request are opened to merge into `master`.

![OpenFlexure flow](https://gitlab.com/openflexure/openflexure-identity/raw/master/flow.png)

## Releases

* Once we've merged a few things in and want to make a release, we tag it on `master`.
* Not every commit on `master` will get a tag, we'll accumulate a few changes before making a release, unless there's a reason to release straight away.
* After a release tag, `master` becomes the development branch of the next significant release.
* Hotfixes for releases may be developed on specific hotfix branches.
