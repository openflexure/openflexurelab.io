#! /usr/bin/env python3

"""
Run as script. Use -h function from argparse for more details
"""

import argparse
import requests
import yaml

# Each feild is a tuple (name in Airtable, variable name in python and website)
# Note some feilds are adjusted further before the website.
EXPECTED_FIELDS = [('Business Name', 'name'),
                   ('Website', 'website'),
                   ('Location Group', 'location_group'),
                   ('Manufacturer Location', 'location'),
                   ('OpenFlexure Microscope Products', 'ofm_products'),
                   ('Other Products', 'other_products'),
                   ('Shipping Terms', 'shipping_terms'),
                   ('About', 'about'),
                   ('Contact Email', 'email'),
                   ('Image', 'image'),
                   ('Notes', 'notes'),
                   ('Logo', 'logo'),
                   ('Product Page Link', 'product_page'),
                   ('OpenFlexure contribution', 'contrib'), # final vars contrib_finance, contrib_project
                   ('Approved', 'approved')]

def get_choices(f_schema):
    """
    get choices from a field schema (only for slect types)
    """
    return [choice['name'] for choice in f_schema['options']['choices']]

def get_field_schema(field_name, schema):
    """
    Return the feild schema for the named feild
    """
    return next(s for s in schema['fields'] if s['name'] == field_name)


def check_schema(schema):
    """
    This will rase an assert error if the schama isn't as expected
    """
    field_names = [f['name'] for f in schema['fields']]
    for field_name, _ in EXPECTED_FIELDS:
        assert field_name in field_names
        f_schema = get_field_schema(field_name, schema)
        field_type = f_schema['type']

        # Check some specific cases we rely on
        if field_name == 'OpenFlexure contribution':
            assert field_type == 'multipleSelects'
            assert 'Sales percentage contributed' in get_choices(f_schema)
            assert 'Percentage contributed for Sangaboard' in get_choices(f_schema)
            assert 'Contributes to the open project' in get_choices(f_schema)


def clean_data(vendors, schema):
    """
    Adjusting the data before we filter as airtable annoying doesn't return
    anything for empty feilds making filtering a bit annoying than
    `[vendor for vendor in vendors if vendor['Approved']]`
    """
    clean_vendor_list = []

    for vendor in vendors:
        cleaned_vendor = {}
        #loop over expected feilds ...
        for field_name, var_name in EXPECTED_FIELDS:
            if field_name == 'Business Name':
                name = vendor.get(field_name, '')
                cleaned_vendor['is_sanga'] = name == 'Bongo Tech & Research Labs'              
            ## First handle special cases
            if field_name == 'OpenFlexure contribution':
                contribs = vendor.get('OpenFlexure contribution', [])
                cleaned_vendor['contrib_finance'] = 'Sales percentage contributed' in contribs
                cleaned_vendor['contrib_project'] = 'Contributes to the open project' in contribs
                cleaned_vendor['contrib_sanga'] = 'Percentage contributed for Sangaboard' in contribs
                continue

            field_type = get_field_schema(field_name, schema)['type']
            if field_type == 'checkbox':
                default = False
            elif field_type == 'multipleSelects':
                default = []
            else:
                #Other types are strings
                default = ''
            cleaned_vendor[var_name] = vendor.get(field_name, default)
        clean_vendor_list.append(cleaned_vendor)
    return clean_vendor_list

def get_data_from_server(base_id, table_id, access_token):
    """
    Get data from airtable
    """
    record_url = f"https://api.airtable.com/v0/{base_id}/{table_id}"
    schema_url = f"https://api.airtable.com/v0/meta/bases/{base_id}/tables"

    result = requests.get(
        schema_url,
        headers={"Authorization": f"Bearer {access_token}"},
        timeout=10)

    assert result.status_code == 200, "Couldn't access schema"
    schema = result.json()
    schema = next(t for t in schema['tables'] if t['id'] == table_id)

    result = requests.get(
        record_url,
        headers={"Authorization": f"Bearer {access_token}"},
        timeout=10)

    assert result.status_code == 200, "Couldn't access records"

    vendors = [record['fields'] for record in result.json()['records']]
    return schema, vendors

def main():
    """
    Get data from airtable, validate, clean and export to _data/vendors.yml
    """
    parser = argparse.ArgumentParser(
        prog='OpenFlexure Vendor Page Generator',
        description='Generates the vensor list from OpenScience Shop')
    parser.add_argument(
        '--show-unapproved',
        '-u',
        action='store_true',
        help="Show unapproved (for testng)")
    parser.add_argument('base_id')
    parser.add_argument('table_id')
    parser.add_argument('token')

    args = parser.parse_args()

    schema, vendors = get_data_from_server(
            base_id=args.base_id,
            table_id=args.table_id,
            access_token=args.token)

    check_schema(schema)
    vendors = clean_data(vendors, schema)
    
    # Remove unapproved vendors unless flag is set
    if not args.show_unapproved:
        vendors = [v for v in vendors if v['approved']]
    # Only export vendors that sell OFMs and have a name and website set
    vendors = [v for v in vendors if v['name'] and v['website'] and v['email']]

    with open('_data/vendors.yml', 'w', encoding="utf-8") as file_obj:
        file_obj.write(yaml.dump(vendors))

if __name__ == "__main__":
    main()
