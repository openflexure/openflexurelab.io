---
layout: home
---

<div class='embedded-youtube'><iframe src='https://www.youtube.com/embed/InmLDDsRmb4' frameborder='0' allowfullscreen></iframe></div>

<div class="flex-container flex-container-horizontal">
    {% for home_card in site.home_cards %}
        {% include homecard.html link=home_card.link title=home_card.title content=home_card.content hero=home_card.hero margin=home_card.margin %}
    {% endfor %}
</div>

## Images taken using OpenFlexure devices
{: .text-center .padding-small}
{% include carousel.html height="70" unit="%" thumbnail="True"%}

## Our hardware
{: .text-center .padding-small}
<div class="flex-container">
    <div class="flex-grow flex-container flex-container-vertical margin-left-very-small">
        {% for home_card in site.home_cards_devices %}
            {% include homecard.html link=home_card.link title=home_card.title content=home_card.content hero=home_card.hero%}
        {% endfor %}
    </div>
</div>