---
layout: post
title:  "Using OpenFlexure to build an accessible Structured Illumination Microscopy (SIM) system"
categories: openflexurecon2022 community_stories
tldr: "Dr. Tatsunosuke Matsui (Mie University, Japan) built an accessible SIM system, adapting the OpenFlexure Microscope design."
date: 2022-10-20
image: /assets/openflexurecon2022/OpenFlexureCon2022_japan.jpg
---

One of the beauties of open source is that people all around the world can learn about your project, take whatever they feel it’s useful for them, and do something new with it. Nowadays in software projects it’s quite easy to be aware of these derivatives; most online repositories track them as “forks”. Forks are often considered a measure of impact of a project; they reflect the interest of people in building on top of the original project idea. 

In open hardware things work slightly different; there’s no one central repository for projects and it’s quite hard to track if someone reuses a part of your 3D-printed design for something else. In complex projects like the OpenFlexure Microscope, people can find different components useful for a diversity of purposes. But what may seem to be a challenge is also an opportunity for unexpected discoveries.  

That’s precisely what happened last June, when the OpenFlexure team at University of Bath learned about a new paper published in Optics Express. [The article] was written by authors **Tatsunosuke Matsui** and **Daigo Fujiwara** at Mie University, Japan; no one in the OpenFlexure team had been in contact with them before. The paper described how the researchers built an accessible and high-resolution Structured Illumination Microscope (SIM) by adapting parts of the OpenFlexure Microscope.  

The timing was perfect. Tatsunosuke Matsui was invited to share his work at OpenFlexure Con, the first community gathering taking place at Bath in July 2022. Dr. Matsui first explained how structured illumination microscopy (SIM) works. By introducing a grid of line and space patters and a piezo stage, SIM can produce better quality images than conventional microscopy. Images of the sample are taken at fixed shifts of the grid’s spatial phase; post-processing is used to distinguish the well-focused image from the blurry one. This method, called “optical sectioning” allows SIM to reach a few microns depth resolution. 

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/YjLOR5VbISg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

SIM allows researchers to achieve super resolution while using thicker sections, turning it into a valuable tool for a diversity of applications. Particularly in biology it has become very popular for live cell imaging. However, the piezo stage needed for SIM is quite expensive, restricting access to the technique to well-funded researchers and institutions.  

The team at MEI University adapted the OpenFlexure 3D-printed stages to replace the expensive piezo stage. They combined versions of the two OFM designs: the OpenFlexure Delta Stage for positioning the sample, and a modified OpenFlexure Block Stage for actuation of the grid. In this way they ensured the grid was positioned and actuated in the conjugate plane with the sample, a mandatory requirement for SIM to work.  

As a result, Matsui & Fujiwara successfully built an accessible, 3D-printed robotic SIM based on OpenFlexure, capable of producing images of a few microns depth resolution. At OpenFlexureCon, Dr. Matsui highlighted the value of so-called “hobbyist” electronics for widening access to science. At OpenFlexure we share his enthusiasm, and we are excited for the new science  

All the necessary files for building the OpenFlexure-based SIM are open source, available at Dr. Matsui’s GitHub repository: https://github.com/tatsunosukematsui/.stl-files-for-3D-printing-Daigo   

[The article]: https://doi.org/10.1364/OE.461910 "The article published in Optics Express (open access)"