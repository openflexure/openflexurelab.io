---
layout: post
title:  "Measuring microplastics everywhere using modified OpenFlexure Microscopes"
categories: openflexurecon2022 community_stories
tldr: "Niamh Burke (University College Dublin, Ireland) has modified the OpenFlexure Microscope to enable wide access to sea microplastics monitoring."
date: 2022-10-20
image: /assets/openflexurecon2022/OpenFlexureCon2022_ireland_and_uk.jpg
---

Microplastics are a concern all around the world. The term was coined in 2004 to describe plastic particles smaller than 5 millimeters across, and since them they were found almost everywhere: in deep oceans, drinking water, food, to name a few. Commercial product development and degradation of bigger plastics are the main sources of microplastics, which can be harmful to the environment and animal health. 

**Niamh Burke** is a PhD student at the Pickering Lab, University College Dublin, Ireland. She is developing tools for increasing access to measuring microplastics in the sea. At OpenFlexureCon she explained that one of the problems with microplastics is that we have no baseline, and that we need to start quantifying them literally all around the world. However, the technique used to determine concentration of microplastics is FTIR spectroscopy, which is quite expensive and not practical to use out in the field. 

Microplastics concentration can be assessed directly, by counting samples collected from the sea, or indirectly by looking at comb jellies’ guts. These small animals are good indicators of the concentration of microplastics in their environment, as they tend to accumulate them in their tissues. Thinking of alternative techniques, Niamh started looking for imaging tools capable of scanning good quality images efficiently in the field. 

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/t2t2Q7hI5wY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Niamh found out that what she need was a dual-view field microscope which allowed low and high magnification at the same time. Being no microscopy expert, she found the OpenFlexure Microscope documentation and started tinkering with the 3D-printed actuator. She decided to modify the OFM actuator to combine it with a moving mechanism inspired by 3D-printers themselves and obtained a successful proof of concept. The device provides both low and high magnification at the same time. 

But she didn’t stop there! Niamh also built a belt tensioner that improves usability of the basic OpenFlexure Microscope design. Now that the concept works, she is simplifying the combined design to reduce its cost and make it efficient to use in the field. She expects the devices to become an accessible solution, allowing almost anyone to map concentration of microplastics, anywhere. 

 