---
layout: post
title:  "Assessing soil health with farmers in Argentina, using OpenFlexure Microscopes"
categories: openflexurecon2022 community_stories
tldr: "Fernando Castro (reGOSH / Cooperativa Ayllu, Argentina) is using OpenFlexure Microscopes to help farmers understand the impact of their more sustainable farming practices in Argentina."
date: 2022-10-20
image: /assets/openflexurecon2022/OpenFlexureCon2022_argentina.jpg
---

Industrial agriculture was considered a promising paradigm until not so long ago; but now the environmental impacts of the practice are changing those views. The high yields derived from intensive use of soil and agrochemicals come at the price of pest and weed resistance, water pollution, less biodiversity, food poisoning and soil degradation among others. These consequences are particularly visible in countries with large extensions of industrial agriculture practices, like Argentina.  

Alerted by the impacts in the environment and food provision, some farmers are transitioning towards more environmental-friendly farming practices. Agroecology, or the application of ecological concepts and principals in farming, is becoming more popular both between farmers and consumers in big cities. One of the premises of Agroecology is that soil is much more than a receptacle of nutrients where plants are grown. It’s considered a living system which thrives with microscopic life when it’s healthy.  

For agroecological farmers and researchers it’s important to assess soil health, as an indicator of ecosystem wellbeing. Instead of conventional fertility analysis, focused on concentration of nutrients, assessing soil health implies looking at the diversity of microscopic life in soil. Which fauna is present, which members of the food network are absent? This analysis gives valuable information about the impact of farming practices. 

At OpenFlexureCon, **Fernando Castro** explained how his team is using OpenFlexure Microscopes to assess soil health with small farmers in Argentina. Fernando is an Argentinian researcher and member of the Latin America Network for Open Science Hardware (reGOSH); he is also part of a rural cooperative in Mendoza, Argentina. Inspired by the work of Dr. Elaine Ingham in the USDA, Fernando decided to run microscopy workshops with other agroecological farmers of his region, to look at microscopic life. 

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/mQ2x6AP6w18" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The team started by building five OpenFlexure Microscopes in different flavours: a basic one using a web cam, an upright version with a RaspberryPi camera, a more complex Delta stage. They connected with the Hackteria network, to learn how to use microscopes to assess soil life. As a result, they produced a Zine in Spanish that works as a guide during the workshops.  

Fernando mentions how excited everyone was at the workshops, when they could finally see how alive soil is. He highlighted that all versions of the microscope work well in the field and are simple to use, even the web cam, simplest one. Future work includes aiming to quantify soil life, and translating the OpenFlexure documentation to Spanish. In his own words, *“these images can help farmers to reimagine the soil in a different way while supporting us in telling the stories of our own agroecological transition”*.  

 