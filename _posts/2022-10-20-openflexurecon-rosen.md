---
layout: post
title:  "Enabling digital pathology for Global Health with OpenFlexure Microscopes"
categories: openflexurecon2022 community_stories
tldr: "Daniel Rosen (Baylor College of Medicine, US) is using OpenFlexure Microscopes for faster and more efficient cancer diagnosis around the world."
date: 2022-10-20
image: /assets/openflexurecon2022/OpenFlexureCon2022_usa.jpg
---

Advances in technology make possible for doctors to diagnose disease by examining images in a screen instead of looking through a microscope eyepiece. Glass slides are scanned and converted into high-resolution digital images, that can be viewed on a computer screen or mobile device. If you have enough images and experts, you can train an algorithm to accelerate diagnosis. 

This is part of **Daniel Rosen**’s work at Baylor College of Medicine in the US. Daniel is an MD, a pathologist who works on gastrointestinal cancer diagnosis. In his own words, “I’m the gold standard used to teach the AI algorithm to detect cancer”. Daniel spends hours looking at images and labels those that test positive, so the algorithm knows what to look for.  

Daniel’s workflow functions pretty well when he is at his lab in Texas. The lab has a whole slide imaging (WSI) system, a combination of digital microscope and computer, that allows him to scan samples and easily store and examine the high-resolution images. But the situation changes when he’s abroad, conducting diagnosis as part of the Global Health program at Baylor. 

When Daniel visits a health center in Honduras, after biopsy samples are collected it can take up to a week to process them to obtain good quality glass slides. By that time, he is usually back home in the US. The workaround is to diagnose using whatever image of the glass slides he can get, often taken with a smartphone. Although this is better than nothing, these images are often low-quality ones, and compromise readings.  

It would be ideal to have WSI systems everywhere. However, these cost half a million dollars, are not portable and have complex network and infrastructure requirements. Daniel realized he needed a low-cost scanner which could provide high image resolution, was energy efficient and had low IT infrastructure, energy requirements and operating costs. Enter the OpenFlexure Microscope! 

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/kquKK2eL4tU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

At OpenFlexureCon, Daniel shared how he trained himself to build OpenFlexure Microscopes using the project documentation, 3D printers and Raspberry Pis. He now takes microscopes with him when he goes abroad; they are extremely portable and do not demand lots of infrastructure to work. But the most important feature is that the images scanned with the OFMs are of incredibly good quality and high resolution.  

Beyond Global Health. Daniel’s enthusiasm led him to build and use OpenFlexure Microscopes in Texas to teach pathology to his residents and medical students. He used the images from the OFM to build five educational websites, currently in use at Baylor. And more recently he has used it for high impact research, in a study of cervical cytology smears with African patients. He used the microscopes to assess intra-observer variability in 60 Pap smears. In his own words, *“I’m a huge fan of the project; it’s become essential for my daily work”*.  