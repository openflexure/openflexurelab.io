---
layout: post
title:  "Accelerating malaria diagnosis in Tanzania with the OpenFlexure Microscope"
categories: openflexurecon2022 community_stories
tldr: "Joram Mduda (Ifakara Health Institute, Tanzania) and Paul Nyakyi (Bongo Tech & Research Labs, Tanzania) are using OpenFlexure towards faster, sustainable malaria diagnosis in Tanzania."
date: 2022-10-20
image: /assets/openflexurecon2022/OpenFlexureCon2022_tanzania.jpg
---

Malaria is a serious but curable infection spread by mosquitoes. If treated, people who have malaria can be cured; if it's not diagnosed quickly, it can be fatal. Malaria killed 627,000 people in 2021; most were young children in sub-Saharan Africa. African authorities are increasingly using rapid detection tests to diagnose malaria. Although faster, this method is not recommended by the World Health Organization due to its lower efficacy rates.  

The gold standard method for malaria diagnosis is taking a blood sample and looking at it with a microscope. But in African countries microscopes are difficult to obtain. Collecting and analysing samples is time-consuming when there are few instruments around. When microscopes break down for any reason, the costs of repairing them usually turn them obsolete. 

**Joram Mduda** works as a microscopy technician at the Ifakara Health Institute (IHI), a world class health research organization in Tanzania. IHI is renowned for its work in Malaria amongst a diversity of local health challenges, providing research, training and services. At IHI, one of Joram’s tasks is to process and analyse blood samples from different settings in Tanzania, to diagnose Malaria. 

At OpenFlexureCon, Joram shared his role in a collaboration between IHI and the OpenFlexure team at Bath. He works as a tester of the OpenFlexure Microscope in a real-life setting, to understand what works and what can be improved. To improve image quality, Joram has been working on scanning sample images and labelling them as useful or not for diagnosis. The team at Bath then compares his assessment with the automated detection processes, to improve the code.  

Testing the microscopes is only a part of Joram’s work. He actively showcases the project to local authorities through seminars, to colleagues at IHI, or to the public during open events like the Malaria Day. He mentioned that now OpenFlexure Microscopes can “self-diagnose” software problems during the setup, so it’s much easier to detect errors before proceeding. Current work on the microscope software aims to automate even more steps, so the setup is as easy as possible. “Local authorities are enthusiastic and see the potential of having accessible microscopes produced here, that can be used for improving diagnosis” he shares. 

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/joDKMM3xalY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The microscopes tested by Joram at IHI are built locally at the BongoTech & Research Labs makerspace. Instead of paying overpriced shipping and import costs or having to deal with bureaucracy, OpenFlexure Microscopes are supplied and maintained in Tanzania by Tanzanians.  

**Paul Thomas Nyakyi**, one of the co-founders of BongoTech & Research Labs, shared some of the organisation work at OpenFlexureCon. The makerspace mission is to provide cutting edge technology to respond to the needs of Tanzania; up to 80% of the tools are made by the team. They are active co-developers of the OpenFlexure Microscope, which they rebranded as sayansiScope for the local market.  

BongoTech fully-developed the sangaboard, an electronic board controlling the OpenFlexure Microscope motors. They also created an educational version using a dash cam, which they have trialed in three schools in Dar es Salaam. Beyond the technical, Paul mentioned that BongoTech is now registered as a medical device manufacturer, a step towards the medical certification of the microscope.  This significant milestone will allow BongoTech to officially supply clinics with OpenFlexure Microscopes after the trials at IHI.  

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/IvbjKBfaWDk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>