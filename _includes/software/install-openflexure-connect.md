OpenFlexure Connect is an electron-based client for the OpenFlexure Microscope. It allows for discovery of microscopes on the network and launches the Web App for control of the microscope and basic capture management.

### Download OpenFlexure Connect

There is no need to download OpenFlexure Connect onto your Raspberry Pi. It's already included with [Raspbian-OpenFlexure](raspbian.md).

**Windows**

<div class="flex-container-very-tight">
    <div><a class="download-btn width-s-m" href="https://build.openflexure.org/openflexure-ev/latest/win">Installer (x86-64)</a></div>
    <div><a class="download-btn outline width-s-m" href="https://build.openflexure.org/openflexure-ev/latest/win/portable">Portable (x86-64)</a></div>
</div>

<a target="_blank" href="https://chocolatey.org/packages/openflexure-connect"><img alt="Get it from the Chocolatey package manager" src="{{ '/assets/software/choco-white.svg' | absolute_url }}" /></a>

**Linux**

<div class="flex-container-very-tight">
    <div><a class="download-btn width-s-m" href="https://build.openflexure.org/openflexure-ev/latest/linux/x86_64">AppImage (x86-64)</a></div>
    <div><a class="download-btn outline width-s-m" href="https://build.openflexure.org/openflexure-ev/latest/linux/armv7l">AppImage (ARM)</a></div>
</div>

<a target="_blank" href="https://snapcraft.io/openflexure-connect"><img alt="Get it from the Snap Store" src="https://snapcraft.io/static/images/badges/en/snap-store-white.svg" /></a>

**Mac OS**

Currently, OpenFlexure Connect for Mac OS must be built from source. Instructions can be found in the [Readme file on GitLab](https://gitlab.com/openflexure/openflexure-connect).

### Use a Web Browser

Alternatively, you can access the OpenFlexure graphical interface from any web browser on a computer connected to the same network as your microscope.

You'll need to know either your microscope's IP address, or it's hostname (by default `microscope.local`).

The interface runs on port 5000, and so can be accessed from either `http://microscope.local:5000` or `http://{your microscope's IP address}:5000` (inserting your IP address, for example `http://192.168.1.60:5000`).