### Download Raspbian-OpenFlexure

The simplest way to set up the Raspberry Pi is to download the pre-built SD card image that contains a full operating system with all the necessary software (the OpenFlexure server and OpenFlexure Connect) pre-installed.

<div class="flex-container-tight">

  <div>
    <p><a target="_blank" class="download-btn" href="https://build.openflexure.org/raspbian-openflexure/armhf/latest">Raspbian OpenFlexure</a></p>
    <p>Full desktop OS.</p>
    <p>Includes full graphical microscope control software.</p>
    <p><b>Required:</b></p>
    <p>Raspberry Pi 2 Model B (1GB RAM)
    <br><i>(or better)</i></p>
    <p><b>Recommended:</b></p>
    <p>Raspberry Pi 4 Model B (2GB RAM)
    <br><i>(or better. 8GB model not supported)</i></p>
  </div>

  <div>
    <p><a target="_blank" class="download-btn" href="https://build.openflexure.org/raspbian-openflexure/armhf/lite/latest">Raspbian OpenFlexure Lite</a></p>
    <p>Minimal OS without desktop.</p>
    <p>You will need another computer to control the microscope graphically.</p>
    <p><b>Required:</b></p>
    <p>Raspberry Pi 1 Model B+ (512mb RAM)
    <br><i>(or better)</i></p>
    <p><b>Recommended:</b></p>
    <p>Raspberry Pi 2 Model B (1GB RAM)
    <br><i>(or better. 8GB model not supported)</i></p>
  </div>

</div>

### Write your SD card

A guide explaining how to install a Raspberry Pi operating system image onto an SD card can be found [here on the Raspberry Pi website](https://www.raspberrypi.com/documentation/computers/getting-started.html#installing-the-operating-system). You will need another computer with an SD card reader to install the image.

The default username is `pi`, with a default password `openflexure`.

### First boot

Connect your Pi to a display, mouse, keyboard, and power. When the Pi first boots, you will be asked to complete a quick setup, before rebooting.

After rebooting, the OpenFlexure Microscope Server should run automatically.
You no longer need to connect the microscope to a display or peripherals if you're using the microscope from another computer. The server application will handle all communication to the microscope.
