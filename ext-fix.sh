#! /usr/bin/bash
cd $1
for file in $(find . -type f -regex ".*\.html"); do
  perl -i -pe 's>(href="(?:\.\/|\/)?(?:[^":\/\n]+\/)*[^":\/\.\n]+)">$1.html">g' $file
  perl -i -pe 's>(href="(?:(?:\/|\.\/)?[^":\/\n]+)*\/)">$1index.html">g' $file
  perl -i -pe "s>(href=\")(\/(?!-).*\")>\$1$2\$2>g" $file
done
