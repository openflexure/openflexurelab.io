---
title: OpenFlexure Delta Stage
hero: '/assets/icons/deltastage.svg'
link: '/projects/deltastage'
---

A 3D-printed delta-geometry stage, suitable for motorised microscopy with maximum stability.
