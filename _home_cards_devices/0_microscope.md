---
title: OpenFlexure Microscope
hero: '/assets/icons/microscope.svg'
link: '/projects/microscope/'
---

An open-source, 3D-printed microscope, including a precise mechanical stage to move the sample and focus the optics.
