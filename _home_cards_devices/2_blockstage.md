---
title: OpenFlexure Block Stage
hero: '/assets/icons/blockstage.svg'
link: '/projects/blockstage'
---

A 3D printable stage including sub-micron mechanical positioning, with a focus on good mechanical stability.
