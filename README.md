# [Openflexure.org](https://openflexure.org)

![Build Status](https://gitlab.com/openflexure/openflexure.gitlab.io/badges/master/pipeline.svg)
![Jekyll Version](https://img.shields.io/gem/v/jekyll.svg)


This is the website source for the Openflexure project. It is a static website generated with Jekyll and hosted on Gitlab Pages. Generic help and information about using Jeykyll and Gitlab Pages below.


# Jekyll and Gitlab Pages

[Learn more about GitLab Pages](https://pages.gitlab.io) or read the the [official GitLab Pages documentation](https://docs.gitlab.com/ce/user/project/pages/).


## Using Jekyll locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Jekyll
1. Download dependencies: `bundle`
1. Build and preview: `bundle exec jekyll serve`
1. Add content

The above commands should be executed from the root directory of this project.

Read more at Jekyll's [documentation][].

## Previewing with Docker

A really convenient way to preview the project using Docker is to run:

`docker run -p 4000:4000 -v "`pwd`:/site" bretfisher/jekyll-serve`

or, on Windows,

`docker run -p 4000:4000 -v "${pwd}:/site" bretfisher/jekyll-serve`

This will spin up a Docker container, start a Jekyll development server, and serve the current directory on `http://localhost:4000/`.  This works better than just previewing the built HTML files, because the server root is in the right place.  It does require you to have Docker, though.

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):


## Troubleshooting

1. CSS is missing! That means two things:
    * Either that you have wrongly set up the CSS URL in your templates, or
    * your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.

[ci]: https://about.gitlab.com/gitlab-ci/
[Jekyll]: http://jekyllrb.com/
[install]: https://jekyllrb.com/docs/installation/
[documentation]: https://jekyllrb.com/docs/home/
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
